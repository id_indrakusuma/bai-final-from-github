<?php
/*
	Controller ini digunakan untuk customisasi Frontend dari Website BAI SYSTEM ONLINE
	Fitur pada controller ini adalah :
	- Menambah / mengganti SLideshow
	- Mengubah Kata Sambuatan
	- Mengubah Kenapa harus gabung BAI ?
	- Tentang BAI
	- Informasi Kontak BAI (Nomor Telp, Email, Alamat Kantor, Jam Buka)
	- Sosial Media BAI (LINE, Facebook, Twitter, Instagram)
	- Info Tambahan

*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Website extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!isset($_SESSION)){
            session_start();
        }
        if ($_SESSION['role_user'] != 'superadmin' && $_SESSION['role_user'] != 'admin') {
        	redirect(base_url());
        }
	}

	public function index()
	{
		$data['title']		= "Slideshow Website";
		$data['custom_css']	= "data_tables_css";
    	$data['custom_js']	= "data_tables_js";
    	$data['custom_script']	= "script_website_js";
    	$data['content']	= "website/slide_show";

		$this->load->view('Main', $data);
	}

	public function galeri()
	{
		$data['title']		= "Slideshow Website";
		$data['custom_css']	= "data_tables_css";
    	$data['custom_js']	= "data_tables_js";
    	$data['custom_script']	= "script_website_js";
    	$data['content']	= "website/slide_show";

		$this->load->view('Main', $data);
	}

}

/* End of file Website.php */
/* Location: ./application/controllers/Website.php */
