<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grud extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!isset($_SESSION)){
            session_start();
        }
        if ($_SESSION['role_user'] != "admin" && $_SESSION['role_user'] != "superadmin") {
        	redirect(base_url());
        }

        //load library GRUCERY CRUD

        $this->load->library('grocery_CRUD');
        $this->crud = new grocery_CRUD();


	}


	public function event()
	{
		echo $_SESSION['role_user'];
		$user_aktif = $_SESSION['id_user'];
		$this->crud->set_table('event');
		$this->crud->set_subject('Event Besar BAI');

		$this->crud->columns('id_user','nama_event','foto_event','tanggal_pelaksanaan');
		$this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));
		$this->crud->set_relation('id_pe','peserta_event','jenis_pe');

		$this->crud->display_as('id_user', 'Publisher');
		$this->crud->display_as('nama_event', 'Nama');
		$this->crud->display_as('foto_event', 'Foto');
		$this->crud->display_as('tanggal_pelaksanaan', 'Pelaksanaan Event');
		$this->crud->display_as('id_pe', 'Peserta Event');


		$this->crud->required_fields('id_user','nama_event','foto_event','tanggal_pelaksanaan','tag_tambahan','desk_event','id_pe');


		$this->crud->set_field_upload('foto_event', 'uploads');
		$this->crud->callback_before_upload(array($this, '_valid_images'));

		//kondiis untuk membatasi isi field
		//$this->crud->where("event.tanggal_pelaksanaan >=",date('Y-m-d'));

		//API grucery untuk beberapa fungsi
		$this->crud->unset_read();
        $this->crud->unset_export();
        $this->crud->unset_print();

        $output = $this->crud->render();
        $output->gcrud = 1;
        $output->title = "Event BAI";
        $output->content = "content/blank";

		$this->load->view('grucery', $output);


	}
	public function artikel()
	{
		$user_aktif = $_SESSION['id_user'];
		$this->crud->set_table('artikel');
		$this->crud->set_subject('Artikel');

		$this->crud->columns('id_user','judul_artikel', 'id_kategori');
		$this->crud->display_as('id_user', 'Publisher');
		$this->crud->display_as('id_kategori', 'Kategori Artikel');
		$this->crud->required_fields('id_user','id_kategori','isi_artikel');
		//membatasi jumlah user yang akan dipost -> sesuai denga
		$this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));
		$this->crud->set_relation('id_kategori','kategori_artikel','judul_kategori');

		//edit field
		$this->crud->add_fields('id_user','judul_artikel','gambar_artikel','isi_artikel','id_kategori');
		//file yang boleh diedit
		$this->crud->edit_fields('judul_artikel','gambar_artikel','isi_artikel','id_kategori');
		//upload type path
		$this->crud->set_field_upload('gambar_artikel', 'uploads');

		//validasi gambar
		$this->crud->callback_before_upload(array($this, '_valid_images'));

		//kondiis untuk membatasi isi field
		$this->crud->where("artikel.id_user",$_SESSION['id_user']);

		//API grucery untuk beberapa fungsi
		$this->crud->unset_read();
        $this->crud->unset_export();
        $this->crud->unset_print();

        $output = $this->crud->render();
        $output->gcrud = 1;
        $output->title = "Artikel BAI";
        $output->content = "content/blank";

		$this->load->view('grucery', $output);
	}

	//info untuk BAI
	public function tentang()
	{
		$user_aktif = $_SESSION['id_user'];
		$this->crud->set_table('tentang_bai');
		$this->crud->set_subject('Tentang Badan Amalan Islam');

		//kolom yang ditampilkan
		$this->crud->columns('desk_singkat','id_user');
		$this->crud->display_as('id_user', 'Publiser');
		$this->crud->display_as('desk_panjang', 'Tentang BAI');
		//relasi antar tabel
		$this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));
		$this->crud->edit_fields('desk_panjang','gambar','id_user');
		//field yang dibutuhkan
		$this->crud->required_fields('desk_oanjang','gambar','id_user');

		//validasi gambar
		$this->crud->set_field_upload('gambar', 'uploads');
		$this->crud->callback_before_upload(array($this, '_valid_images'));

		//GROCERY API

		$this->crud->unset_add();
		$this->crud->unset_delete();
		$this->crud->unset_read();
        $this->crud->unset_export();
        $this->crud->unset_print();

		$output = $this->crud->render();
		$output->gcrud = 1;
		$output->title = "Tentang Badan Amalan Islam";
		$output->content = "content/blank";
		$this->load->view('grucery', $output);
	}

	public function slideshow()
	{
		$user_aktif = $_SESSION['id_user'];
		$this->crud->set_table('slideshow');
		$this->crud->set_subject('Slideshow Website');

		//kolom yang ditampilkan
		$this->crud->columns('judul_slide','gambar_slide','id_user');
		$this->crud->display_as('id_user', 'Publiser');
		$this->crud->display_as('judul_slide', 'Judul Slideshow');

		//relasi
		$this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));

		//add & edit fields
		$this->crud->add_fields('judul_slide','ket_slide','link_slide', 'gambar_slide','id_user');
		$this->crud->edit_fields('judul_slide','ket_slide','link_slide', 'gambar_slide','id_user');
		$this->crud->required_fields('gambar_slide','id_user');


		//validasi gambar
		$this->crud->set_field_upload('gambar_slide', 'uploads');
		$this->crud->callback_before_upload(array($this, '_valid_images'));


		//grucery API
		$this->crud->unset_read();
        $this->crud->unset_export();
        $this->crud->unset_print();

		$output = $this->crud->render();
		$output->gcrud = 1;
		$output->title = "Slideshow Website";
		$output->content = "content/blank";
		$this->load->view('grucery', $output);

	}

	public function galeri()
	{
		$user_aktif = $_SESSION['id_user'];
		$this->crud->set_table('galeri');
		$this->crud->set_subject('Gallery Kegiatan BAI');

		//kolom yang ditampilkan
		$this->crud->columns('id_user','judul_galeri','foto_galeri','tahun_foto');
		$this->crud->display_as('id_user', 'Publiser');
		$this->crud->display_as('judul_galeri', 'Judul Foto');
		$this->crud->display_as('foto_galeri', 'Foto');
		$this->crud->display_as('tahun_foto', 'Tahun');

		//relasi
		$this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));

		//add & edit fields
		$this->crud->add_fields('id_user','judul_galeri','foto_galeri', 'tahun_foto','keterangan');
		$this->crud->edit_fields('judul_galeri','foto_galeri', 'tahun_foto','keterangan');
		$this->crud->required_fields('judul_galeri','foto_galeri', 'tahun_foto','id_user');


		//validasi gambar
		$this->crud->set_field_upload('foto_galeri', 'uploads');
		$this->crud->callback_before_upload(array($this, '_valid_images'));


		//grucery API
		$this->crud->unset_read();
        $this->crud->unset_export();
        $this->crud->unset_print();

		$output = $this->crud->render();
		$output->gcrud = 1;
		$output->title = "Foto Kegiatan";
		$output->content = "content/blank";
		$this->load->view('grucery', $output);

	}

	public function sambutan()
	{
		$user_aktif = $_SESSION['id_user'];
		$this->crud->set_table('sambutan_ketua');
		$this->crud->set_subject('Sambutan Ketua BAI');

		//kolom yang ditampilkan
		$this->crud->columns('foto_sambutan','nama_ketua','jabatan','id_user');
		$this->crud->display_as('id_user', 'Publiser');
		$this->crud->display_as('foto_sambutan', 'Foto Ketua');
		$this->crud->display_as('nama_ketua', 'Nama Ketua');
		$this->crud->display_as('jabatan', 'Jabatan');
		$this->crud->display_as('isi_sambutan', 'Isi Sambutan');

		//relasi
		$this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));

		//add & edit fields
		$this->crud->add_fields('foto_sambutan','isi_sambutan','nama_ketua','jabatan','id_user');
		$this->crud->edit_fields('foto_sambutan','isi_sambutan','nama_ketua','jabatan');
		$this->crud->required_fields('foto_sambutan','isi_sambutan','nama_ketua','jabatan','id_user');


		//validasi gambar
		$this->crud->set_field_upload('foto_sambutan', 'uploads');
		$this->crud->callback_before_upload(array($this, '_valid_images'));


		//grucery API
		$this->crud->unset_add();
		$this->crud->unset_delete();
		$this->crud->unset_read();
        $this->crud->unset_export();
        $this->crud->unset_print();

		$output = $this->crud->render();
		$output->gcrud = 1;
		$output->title = "Sambutan Ketua BAI";
		$output->content = "content/blank";
		$this->load->view('grucery', $output);

	}


	public function gabung_bai()
	{
		$user_aktif = $_SESSION['id_user'];
		$this->crud->set_table('gabung_bai');
		$this->crud->set_subject('Alasan Bergabung BAI');

		//kolom yang ditampilkan
		$this->crud->columns('judul','id_user');
		$this->crud->display_as('id_user', 'Publiser');
		$this->crud->display_as('judul', 'Alasan Bergabung');

		//relasi
		$this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));

		//add & edit fields
		$this->crud->add_fields('judul','isi_singkat','id_user');
		$this->crud->edit_fields('judul','isi_singkat','id_user');
		$this->crud->required_fields('judul','isi_singkat','id_user');


		//grucery API
		$this->crud->unset_read();
        $this->crud->unset_export();
        $this->crud->unset_print();

		$output = $this->crud->render();
		$output->gcrud = 1;
		$output->title = "Alasan Bergabung BAI";
		$output->content = "content/blank";
		$this->load->view('grucery', $output);

	}

	public function info_contact()
	{
		$user_aktif = $_SESSION['id_user'];
		$this->crud->set_table('info_contact');
		$this->crud->set_subject('Informasi Contact BAI');

		//kolom yang ditampilkan
		$this->crud->columns('no_hp','no_telp','email','fax','alamat');
		$this->crud->display_as('no_hp', 'Nomor HP');
		$this->crud->display_as('no_telp', 'Telepon');
		$this->crud->display_as('no_hp', 'Nomor HP');
		$this->crud->display_as('fax', 'FAX');
		$this->crud->display_as('alamat', 'Alamat');

		//add & edit fields
		$this->crud->add_fields('no_hp','no_telp','email','fax','alamat', 'ket_tambahan');
		$this->crud->edit_fields('no_hp','no_telp','email','fax','alamat','ket_tambahan');
		$this->crud->required_fields('no_hp','no_telp','email','fax','alamat');


		//grucery API
		$this->crud->unset_add();
		$this->crud->unset_delete();
		$this->crud->unset_read();
        $this->crud->unset_export();
        $this->crud->unset_print();

		$output = $this->crud->render();
		$output->gcrud = 1;
		$output->title = "Informasi Contct BAI";
		$output->content = "content/blank";
		$this->load->view('grucery', $output);

	}

	public function kegiatan_rutin()
	{
		$user_aktif = $_SESSION['id_user'];
		$this->crud->set_table('kegiatan_rutin');
		$this->crud->set_subject('Kegiatan Rutin BAI');

		//kolom yang ditampilkan
		$this->crud->columns('nama_kegiatan','hari','jam_kegiatan','cp','id_user');
		$this->crud->display_as('id_user', 'Publiser');
		$this->crud->display_as('nama_kegiatab', 'Kegiatan');
		$this->crud->display_as('hari', 'Hari');
		$this->crud->display_as('jam_kegiatan', 'Jam Mulai');
		$this->crud->display_as('cp', 'Penanggung Jawab');

		$this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));

		//add & edit fields
		$this->crud->add_fields('nama_kegiatan','deskripsi','hari','jam_kegiatan','cp','id_user', 'logo_icon');
		$this->crud->edit_fields('nama_kegiatan','deskripsi','hari','jam_kegiatan','cp','id_user', 'logo_icon');
		$this->crud->required_fields('nama_kegiatan','deskripsi','hari','jam_kegiatan','cp','id_user', 'logo_icon');

		//


		//grucery API
		$this->crud->unset_delete();
		$this->crud->unset_read();
        $this->crud->unset_export();
        $this->crud->unset_print();

		$output = $this->crud->render();
		$output->gcrud = 1;
		$output->title = "Informasi Contact BAI";
		$output->content = "content/blank";
		$this->load->view('grucery', $output);

	}

	public function faq()
	{
		$user_aktif = $_SESSION['id_user'];
		$this->crud->set_table('faq');
		$this->crud->set_subject('Frequent Ask Question BAI');

		//kolom yang ditampilkan
		$this->crud->columns('judul_pertanyaan','penjelasan','id_user');
		$this->crud->display_as('id_user', 'Publiser');
		$this->crud->display_as('judul_pertanyaan', 'Pertanyaan');
		$this->crud->display_as('penjelasan', 'Penjelasan');
		$this->crud->display_as('waktu', 'Dipublikasi');

		$this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));

		//add & edit fields
		$this->crud->edit_fields('judul_pertanyaan','penjelasan','id_user');
		$this->crud->required_fields('judul_pertanyaan','penjelasan','id_user');

		//


		//grucery API
		$this->crud->unset_delete();
		$this->crud->unset_read();
        $this->crud->unset_export();
        $this->crud->unset_print();

		$output = $this->crud->render();
		$output->gcrud = 1;
		$output->title = "FAQ BAI";
		$output->content = "content/blank";
		$this->load->view('grucery', $output);

	}

	public function feedback()
	{
		$this->crud->set_table('contact_us');
		$this->crud->set_subject('Feedback Dari Pengguna');

		//kolom yang ditampilkan
		$this->crud->columns('nama','email','telepon', 'pesan');
		$this->crud->display_as('nama', 'Nama');
		$this->crud->display_as('email', 'Email');
		$this->crud->display_as('telepon', 'Nomor Telpon');
		$this->crud->display_as('pesan', 'Pesan / Feedback');

		//add & edit fields

		//


		//grucery API
		$this->crud->unset_delete();
		$this->crud->unset_add();
		$this->crud->unset_edit();
        $this->crud->unset_export();
        $this->crud->unset_print();

		$output = $this->crud->render();
		$output->gcrud = 1;
		$output->title = "FAQ BAI";
		$output->content = "content/blank";
		$this->load->view('grucery', $output);

	}



	//callback upload gambar || file yang diijinkan PNG|JPG|JPEG -> bisa ditambahi lagi, tinggal diberikan kondiis tambahan
		public function _valid_images($files_to_upload, $field_info)
		{
		  if ($files_to_upload[$field_info->encrypted_field_name]['type'] != ('image/png' || 'image/jpg' || 'image/jpeg'))
		  {
		   	return 'Maaf ! Gambar yang diperbolehkan hanya bertipe PNG / JPG / JPEG';
		  }
		  return true;
		}

}

/* End of file Grud.php */
/* Location: ./application/controllers/Grud.php */
