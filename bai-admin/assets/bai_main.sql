/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.6.21 : Database - bai_main
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bai_main` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bai_main`;

/*Table structure for table `artikel` */

DROP TABLE IF EXISTS `artikel`;

CREATE TABLE `artikel` (
  `id_artikel` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `judul_artikel` varchar(200) DEFAULT NULL,
  `isi_artikel` text,
  `gambar_artikel` varchar(200) DEFAULT NULL,
  `tanggal_post` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_kategori` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_artikel`),
  KEY `id_user` (`id_user`),
  KEY `id_kategori` (`id_kategori`),
  CONSTRAINT `artikel_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `artikel_ibfk_2` FOREIGN KEY (`id_kategori`) REFERENCES `kategori_artikel` (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `artikel` */

insert  into `artikel`(`id_artikel`,`id_user`,`judul_artikel`,`isi_artikel`,`gambar_artikel`,`tanggal_post`,`id_kategori`) values (1,1,'Menganalisa Perkembangan Jaman','hai hai hai test hai hai hai test\r\nhai hai hai test\r\nhai hai hai test\r\nhai hai hai test\r\nhai hai hai test\r\nhai hai hai test\r\n\r\n','---','2016-10-04 23:26:20',2);

/*Table structure for table `contact_us` */

DROP TABLE IF EXISTS `contact_us`;

CREATE TABLE `contact_us` (
  `id_pesan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `pesan` text,
  `status_p` enum('0','1') DEFAULT '0',
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_pesan`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `contact_us_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `contact_us` */

insert  into `contact_us`(`id_pesan`,`nama`,`email`,`telepon`,`pesan`,`status_p`,`id_user`) values (5,'Indra','Indra ','iNdra','askaksjaksjasMessage...','0',NULL),(6,'12','121','12121','12121212Message...','0',NULL);

/*Table structure for table `gabung_bai` */

DROP TABLE IF EXISTS `gabung_bai`;

CREATE TABLE `gabung_bai` (
  `id_gabung` int(3) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) DEFAULT NULL,
  `isi_singkat` varchar(200) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_gabung`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `gabung_bai_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `gabung_bai` */

/*Table structure for table `jenis_kegiatan` */

DROP TABLE IF EXISTS `jenis_kegiatan`;

CREATE TABLE `jenis_kegiatan` (
  `id_jk` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_kegiatan` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_jk`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `jenis_kegiatan` */

insert  into `jenis_kegiatan`(`id_jk`,`jenis_kegiatan`) values (1,'RKT'),(2,'Non-RKT');

/*Table structure for table `kategori_artikel` */

DROP TABLE IF EXISTS `kategori_artikel`;

CREATE TABLE `kategori_artikel` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `judul_kategori` varchar(100) DEFAULT NULL,
  `status_kategori` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `kategori_artikel` */

insert  into `kategori_artikel`(`id_kategori`,`judul_kategori`,`status_kategori`) values (1,'Agama',''),(2,'Organisasi','1'),(4,'Religi','0'),(5,'Berita BAI','1'),(6,'Kabar UDINUS','1');

/*Table structure for table `kegiatan` */

DROP TABLE IF EXISTS `kegiatan`;

CREATE TABLE `kegiatan` (
  `id_kegiatan` int(4) NOT NULL AUTO_INCREMENT,
  `nama_kegiatan` varchar(100) DEFAULT NULL,
  `tempat` varchar(150) DEFAULT NULL,
  `pj_kegiatan` varchar(100) DEFAULT NULL,
  `deskripsi_kegiatan` text,
  `tanggal_kegiatan` date NOT NULL,
  `jam_mulai` varchar(14) NOT NULL,
  `id_user` int(4) DEFAULT NULL,
  `id_jk` int(4) DEFAULT NULL,
  `id_pe` int(4) DEFAULT NULL,
  PRIMARY KEY (`id_kegiatan`),
  KEY `id_user` (`id_user`),
  KEY `id_jk` (`id_jk`),
  KEY `id_pe` (`id_pe`),
  CONSTRAINT `kegiatan_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `kegiatan_ibfk_2` FOREIGN KEY (`id_jk`) REFERENCES `jenis_kegiatan` (`id_jk`),
  CONSTRAINT `kegiatan_ibfk_3` FOREIGN KEY (`id_pe`) REFERENCES `peserta_event` (`id_pe`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `kegiatan` */

insert  into `kegiatan`(`id_kegiatan`,`nama_kegiatan`,`tempat`,`pj_kegiatan`,`deskripsi_kegiatan`,`tanggal_kegiatan`,`jam_mulai`,`id_user`,`id_jk`,`id_pe`) values (1,'Pengajian Kamis Rutin Lagi','Masjid Gedung D UDINUS','Zakiy Anwar Fakhri','Bertempat di Mushola An-Nur Pengadilan Negeri Bale Bandung, diadakan pengajian rutin setiap hari kamis, acara tersebut diadakan oleh DKM Mushola An-Nur Pengadilan Negeri Bale Bandung, acara diisi oleh tanya jawab untuk meningkatkan keimanan. Acara tersebut dihadiri oleh Wakil Ketua Pengadilan Negeri Bale Bandung Ibu Hj. DIAH SULASTRI DEWI, SH., MH., Pejabat Struktural, Hakim, Pegawai dan Honorer.\r\n\r\n<div><br></div><div>\r\n\r\nBertempat di Mesjid Babussalam Pengadilan Negeri Bale Bandung, dilaksanakan peletakkan batu pertama pemugaran mesjid Babussalam. Peletakkan batu pertama diawali oleh Ketua Pengadilan Negeri Bale Bandung SUHARTANTO, SH.,MH, dilanjutkan oleh Wakil Ketua PUJIASTUTI HANDAYANI, SH.,MH dan diakhiri oleh Panitera Dr.H. ASEP DEDI SUWASTA, SH.MH sebagai Ketua Pelaksana renovasi dan pemugaran Mesjid Babussalam.\r\n\r\n<br></div>','2016-10-12','11:00 AM',1,1,1),(6,'Islamic Fair','Universitas Dian Nuswantoro','Kharis Hilmi Muhammad','<p>Kharis Hilmi Muhammad&nbsp;Kharis Hilmi Muhammad&nbsp;Kharis Hilmi MuhammadKharis Hilmi Muhammad<br></p>','2016-10-14','08:00 AM',1,1,3),(7,'BAI Open House 2016','Bumi Perkemahan Semarang','Zakiy Anwar Fakhri','<p>Kharis Hilmi MuhammadKharis Hilmi MuhammadKharis Hilmi MuhammadKharis Hilmi MuhammadKharis Hilmi MuhammadKharis Hilmi MuhammadKharis Hilmi MuhammadKharis Hilmi MuhammadKharis Hilmi Muhammad<br></p>','2016-10-11','04:00 PM',1,1,2);

/*Table structure for table `peserta_event` */

DROP TABLE IF EXISTS `peserta_event`;

CREATE TABLE `peserta_event` (
  `id_pe` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pe` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_pe`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `peserta_event` */

insert  into `peserta_event`(`id_pe`,`jenis_pe`) values (1,'Pengurus BAI'),(2,'Anggota BAI'),(3,'UMUM'),(4,'SMA/SMK/Se-Derajat'),(5,'SMP/SLTP'),(6,'TK/SD');

/*Table structure for table `sambutan_ketua` */

DROP TABLE IF EXISTS `sambutan_ketua`;

CREATE TABLE `sambutan_ketua` (
  `id_sambutan` int(3) NOT NULL AUTO_INCREMENT,
  `foto_sambutan` varchar(100) DEFAULT NULL,
  `isi_sambutan` text,
  `nama_ketua` varchar(100) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_sambutan`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `sambutan_ketua_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sambutan_ketua` */

/*Table structure for table `slideshow` */

DROP TABLE IF EXISTS `slideshow`;

CREATE TABLE `slideshow` (
  `id_slide` int(3) NOT NULL AUTO_INCREMENT,
  `judul_slide` varchar(100) DEFAULT NULL,
  `ket_slide` text,
  `gambar_slide` varchar(100) DEFAULT NULL,
  `link_slide` varchar(200) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `pembuatan` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_slide`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `slideshow_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `slideshow` */

/*Table structure for table `system_log` */

DROP TABLE IF EXISTS `system_log`;

CREATE TABLE `system_log` (
  `id_aktifitas` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `aktifitas` int(11) DEFAULT NULL,
  `tanggal_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_aktifitas`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `system_log_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=latin1;

/*Data for the table `system_log` */

insert  into `system_log`(`id_aktifitas`,`id_user`,`aktifitas`,`tanggal_log`) values (1,1,1,'2016-09-29 12:17:56'),(2,1,0,'2016-09-29 12:19:25'),(3,1,1,'2016-09-29 15:02:56'),(4,1,0,'2016-09-29 15:05:11'),(5,1,1,'2016-09-29 15:10:50'),(6,1,0,'2016-09-29 17:11:07'),(7,1,1,'2016-09-29 17:30:09'),(8,1,0,'2016-09-29 19:11:27'),(9,1,1,'2016-09-29 20:20:37'),(10,1,1,'2016-09-30 11:14:03'),(11,1,0,'2016-09-30 11:26:25'),(12,1,1,'2016-09-30 11:29:50'),(13,1,0,'2016-09-30 11:32:22'),(14,1,1,'2016-09-30 11:32:31'),(15,1,0,'2016-09-30 11:32:38'),(16,2,1,'2016-09-30 11:32:52'),(17,2,0,'2016-09-30 11:34:54'),(18,1,1,'2016-09-30 11:35:04'),(19,2,1,'2016-09-30 14:15:19'),(20,1,1,'2016-10-03 09:22:27'),(21,1,0,'2016-10-03 09:22:49'),(22,2,1,'2016-10-03 09:22:58'),(23,2,0,'2016-10-03 09:23:14'),(24,1,1,'2016-10-03 09:23:22'),(25,1,0,'2016-10-03 09:23:53'),(26,1,1,'2016-10-03 15:05:25'),(27,1,0,'2016-10-03 22:39:44'),(28,1,1,'2016-10-03 22:39:54'),(29,1,1,'2016-10-03 22:48:09'),(30,1,0,'2016-10-03 22:48:58'),(31,1,0,'2016-10-03 22:54:39'),(32,1,1,'2016-10-03 22:55:07'),(33,1,0,'2016-10-03 22:56:34'),(34,1,1,'2016-10-03 22:56:49'),(35,1,0,'2016-10-03 22:56:59'),(36,2,0,'2016-10-03 22:59:10'),(37,3,0,'2016-10-03 23:03:16'),(38,3,1,'2016-10-03 23:04:11'),(39,1,1,'2016-10-03 23:08:46'),(40,1,1,'2016-10-04 11:54:06'),(41,1,1,'2016-10-04 16:12:18'),(42,1,0,'2016-10-04 17:07:38'),(43,1,0,'2016-10-04 17:07:56'),(44,1,1,'2016-10-04 17:08:14'),(45,1,1,'2016-10-04 17:08:32'),(46,1,1,'2016-10-04 17:08:46'),(47,1,1,'2016-10-04 20:44:53'),(48,4,1,'2016-10-04 20:46:47'),(49,4,0,'2016-10-04 20:46:52'),(50,1,1,'2016-10-04 20:53:58'),(51,1,1,'2016-10-04 21:56:06'),(52,1,1,'2016-10-04 22:01:37'),(53,1,1,'2016-10-04 22:02:00'),(54,1,1,'2016-10-05 05:22:15'),(55,1,0,'2016-10-05 08:23:49'),(56,1,1,'2016-10-05 08:25:25'),(57,1,0,'2016-10-05 08:26:41'),(58,1,1,'2016-10-05 08:26:55'),(59,1,1,'2016-10-05 11:05:01'),(60,1,1,'2016-10-05 12:10:58'),(61,1,1,'2016-10-05 12:50:35'),(62,1,1,'2016-10-05 21:46:30'),(63,1,0,'2016-10-05 22:47:07'),(64,1,1,'2016-10-06 11:27:43'),(65,1,1,'2016-10-07 13:18:55'),(66,1,1,'2016-10-08 04:47:58'),(67,1,1,'2016-10-08 05:05:26'),(68,1,1,'2016-10-11 15:05:06'),(69,1,1,'2016-10-12 11:26:40'),(70,1,0,'2016-10-12 15:47:48'),(71,1,1,'2016-10-12 15:57:37'),(72,1,1,'2016-10-12 15:58:00'),(73,1,1,'2016-10-12 16:20:21'),(74,1,1,'2016-10-12 20:48:59'),(75,1,1,'2016-10-12 20:50:45'),(76,1,0,'2016-10-12 20:55:10'),(77,2,1,'2016-10-12 20:55:18'),(78,2,1,'2016-10-12 20:55:31'),(79,2,0,'2016-10-12 20:55:35'),(80,1,1,'2016-10-13 10:19:36'),(81,1,1,'2016-10-13 10:31:46'),(82,1,1,'2016-10-13 10:38:11'),(83,1,1,'2016-10-13 10:40:34'),(84,1,1,'2016-10-13 11:04:18'),(85,1,1,'2016-10-13 16:43:37'),(86,1,1,'2016-10-14 09:08:51'),(87,1,1,'2016-10-17 13:44:59'),(88,1,0,'2016-10-17 13:48:12'),(89,2,1,'2016-10-17 13:48:27'),(90,2,0,'2016-10-17 13:48:36'),(91,1,1,'2016-10-21 15:22:53'),(92,1,1,'2016-10-21 15:22:53'),(93,1,1,'2016-10-21 15:22:54'),(94,1,1,'2016-10-21 15:22:54'),(95,1,1,'2016-10-21 15:22:54'),(96,1,1,'2016-10-21 15:22:54'),(97,1,1,'2016-10-21 15:22:54'),(98,1,1,'2016-10-21 15:22:54'),(99,1,1,'2016-10-21 15:22:54'),(100,1,1,'2016-10-21 15:22:54'),(101,1,1,'2016-10-21 15:22:55'),(102,1,1,'2016-10-21 15:22:55'),(103,1,1,'2016-10-21 15:22:55'),(104,1,1,'2016-10-21 15:22:55'),(105,1,1,'2016-10-21 15:22:55'),(106,1,1,'2016-10-21 15:22:56'),(107,1,1,'2016-10-21 15:22:56'),(108,1,1,'2016-10-21 15:22:56'),(109,1,1,'2016-10-21 15:22:56'),(110,1,1,'2016-10-21 15:22:56'),(111,1,1,'2016-10-21 15:22:56'),(112,1,1,'2016-10-21 15:22:56'),(113,1,1,'2016-10-21 15:22:57'),(114,1,1,'2016-10-21 15:22:57'),(115,1,1,'2016-10-21 15:23:00'),(116,1,1,'2016-10-21 15:23:00'),(117,1,1,'2016-10-21 15:23:00'),(118,1,1,'2016-10-21 15:23:00'),(119,1,1,'2016-10-21 15:23:00'),(120,1,1,'2016-10-21 15:23:00'),(121,1,1,'2016-10-21 15:23:01'),(122,1,1,'2016-10-21 15:23:01'),(123,1,1,'2016-10-21 15:23:01'),(124,1,1,'2016-10-21 15:23:01'),(125,1,1,'2016-10-21 15:23:06'),(126,1,1,'2016-10-21 15:23:06'),(127,1,1,'2016-10-21 15:23:11'),(128,1,1,'2016-10-21 15:23:11'),(129,1,1,'2016-10-21 15:23:11'),(130,1,1,'2016-10-21 15:23:11'),(131,1,1,'2016-10-21 15:23:11'),(132,1,1,'2016-10-21 15:23:11'),(133,1,1,'2016-10-21 15:23:11'),(134,1,1,'2016-10-21 15:23:12'),(135,1,1,'2016-10-21 15:23:12'),(136,1,1,'2016-10-21 15:23:42'),(137,1,1,'2016-10-21 15:23:42'),(138,1,1,'2016-10-21 15:23:42'),(139,1,1,'2016-10-21 15:23:42'),(140,1,1,'2016-10-21 15:23:42'),(141,1,1,'2016-10-21 15:23:43'),(142,1,1,'2016-10-21 15:23:43'),(143,1,1,'2016-10-21 15:23:43'),(144,1,1,'2016-10-21 15:23:43'),(145,1,1,'2016-10-21 15:23:43'),(146,1,1,'2016-10-21 15:23:43'),(147,1,1,'2016-10-21 15:23:52'),(148,1,1,'2016-10-21 15:23:52'),(149,1,1,'2016-10-21 15:23:52'),(150,1,1,'2016-10-21 15:23:52'),(151,1,1,'2016-10-21 15:23:52'),(152,1,1,'2016-10-21 15:23:52'),(153,1,1,'2016-10-21 15:23:52'),(154,1,1,'2016-10-21 15:23:53'),(155,1,1,'2016-10-21 15:23:53'),(156,1,1,'2016-10-21 15:23:53'),(157,1,1,'2016-10-21 15:23:53');

/*Table structure for table `tentang_bai` */

DROP TABLE IF EXISTS `tentang_bai`;

CREATE TABLE `tentang_bai` (
  `id_tentang` int(1) NOT NULL AUTO_INCREMENT,
  `desk_singkat` text,
  `gambar` varchar(100) DEFAULT NULL,
  `desk_panjang` text,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_tentang`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `tentang_bai_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tentang_bai` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama_user` varchar(100) DEFAULT NULL,
  `email_user` varchar(150) DEFAULT NULL,
  `password_user` varchar(40) DEFAULT NULL,
  `role` enum('admin','pengurus') DEFAULT NULL,
  `status_user` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `email` (`email_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`nama_user`,`email_user`,`password_user`,`role`,`status_user`) values (1,'Indra Kusuma','indra@gmail.com','9fb7cae6f9c3aacdb81c1e70b1f843e5','admin','1'),(2,'Zakiy An','zakiy@gmail.com','ef4113dcac30d9fea0cd4ed7caa66ee8','pengurus','1'),(3,'Mita Wanda','mita@gmail.com','bae3d929b274a4cd35c38fe92f059f1a','admin','0'),(4,'Kharis Hilmi Mutakarin','kharis@gmail.com','aa3adc6b720873f0a5a1c273bb75348b','pengurus','1');

/*Table structure for table `detail_acara` */

DROP TABLE IF EXISTS `detail_acara`;

/*!50001 DROP VIEW IF EXISTS `detail_acara` */;
/*!50001 DROP TABLE IF EXISTS `detail_acara` */;

/*!50001 CREATE TABLE  `detail_acara`(
 `id_kegiatan` int(4) ,
 `nama_kegiatan` varchar(100) ,
 `tempat` varchar(150) ,
 `jenis_kegiatan` varchar(40) ,
 `jenis_pe` varchar(40) ,
 `deskripsi_kegiatan` text ,
 `pj_kegiatan` varchar(100) ,
 `jam_mulai` varchar(14) ,
 `tanggal_kegiatan` date ,
 `nama_user` varchar(100) 
)*/;

/*View structure for view detail_acara */

/*!50001 DROP TABLE IF EXISTS `detail_acara` */;
/*!50001 DROP VIEW IF EXISTS `detail_acara` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `detail_acara` AS (select `kegiatan`.`id_kegiatan` AS `id_kegiatan`,`kegiatan`.`nama_kegiatan` AS `nama_kegiatan`,`kegiatan`.`tempat` AS `tempat`,`jenis_kegiatan`.`jenis_kegiatan` AS `jenis_kegiatan`,`peserta_event`.`jenis_pe` AS `jenis_pe`,`kegiatan`.`deskripsi_kegiatan` AS `deskripsi_kegiatan`,`kegiatan`.`pj_kegiatan` AS `pj_kegiatan`,`kegiatan`.`jam_mulai` AS `jam_mulai`,`kegiatan`.`tanggal_kegiatan` AS `tanggal_kegiatan`,`user`.`nama_user` AS `nama_user` from (((`kegiatan` join `user`) join `jenis_kegiatan`) join `peserta_event`) where ((`user`.`id_user` = `kegiatan`.`id_user`) and (`jenis_kegiatan`.`id_jk` = `kegiatan`.`id_jk`) and (`kegiatan`.`id_pe` = `peserta_event`.`id_pe`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
