<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small>BAI Online System</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <div class="row">
        <div class="col-md-8">
          <div class="box box-success">
          <div class="box-header">
              <?php
                $info = $this->session->flashdata('info');
                if (isset($info)) {
                  ?>
                    <div class="callout callout-warning">
                      <h4><?=$info;?></h4>
                    </div>
                  <?php
                }
              ?>
              <h3 class="box-title">Artikel Yang Sudah di Terbitkan</h3>
              <a href="<?=base_url();?>admin/addArtikel" title="" class="btn btn-sm btn-primary btn-flat pull-right"><i class="fa fa-plus-circle"> </i> Tambah Artikel</a>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="user_log" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th>Judul Artikel</th>
                  <th>Penulis</th>
                  <th>Kategori</th>
                  <th>Tanggal Posting</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                
                <tbody>
                  <?php foreach ($this->Main_model->getDaftarArtikel() as $data) {
                    //-> Kondisi tambahan untuk merubah angka menjadi text yang kita inginkan
                    ?>
                      <tr>
                        <td><?=$data['judul_artikel'];?></td>
                        <td><?=$data['nama_user'];?></td>
                        <td><?=$data['judul_kategori'];?></td>
                        <td><?=$data['tanggal_post'];?></td>
                        <td>
                          <a href="<?=base_url();?>admin/ubahArtikel/<?=$data['id_artikel']; ?>" class="btn btn-xs btn-flat btn-warning" title="Ubah Artikel"><i class="fa fa-pencil"></i></a>
                          <a href="<?=base_url();?>admin/hapusArtikel/<?=$data['id_artikel']; ?>" class="btn btn-xs btn-flat btn-danger" title="Hapus Artikel"><i class="fa fa-eraser"></i></a>
                        </td>
                      
                      </tr>
                    <?php
                } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>


        <div class="col-md-4">
          <div class="box box-success">
          <div class="box-header">
              <?php
                $info = $this->session->flashdata('info2');
                if (isset($info)) {
                  ?>
                    <div class="callout callout-warning">
                      <h4><?=$info;?></h4>
                    </div>
                  <?php
                }
              ?>
              <h3 class="box-title">Kategori Artikel</h3>
              <a href="<?=base_url();?>admin/addKategori" title="" class="btn btn-sm btn-flat btn-primary pull-right"><i class="fa fa-plus-circle"> </i> Tambah Kategori</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="kategori" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th>Kategori</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                
                <tbody>
                  <?php foreach ($this->Main_model->getKategoriArtikel() as $data) {
                    //-> Kondisi tambahan untuk merubah angka menjadi text yang kita inginkan
                    ?>
                      <tr>
                        <td><?=$data['judul_kategori'];?></td>
                        <td>
                          <a href="<?=base_url();?>admin/hapusKategori/<?=$data['id_kategori']; ?>" class="btn btn-xs btn-flat btn-danger" title="Hapus Kategori"><i class="fa fa-eraser"></i> Hapus</a>
                        </td>
                      </tr>
                    <?php
                } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>