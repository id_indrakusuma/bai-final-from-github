<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bendahara extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    if(!isset($_SESSION)){
            session_start();
        }
    //cek user role -> yang dizinkan hanya Admin dan bendahara
    $ur = $_SESSION['role_user'];
    if ($ur != 'bendahara' && $ur != 'superadmin') {
          redirect(base_url());
        }
        //load library grucery Grud
        $this->load->library('grocery_CRUD');
        $this->crud = new grocery_CRUD();
  }

  public function index()
  {
    $data['title']		= "Dashboard";
    $data['custom_css']	= null;
    $data['custom_js']	= null;
    $data['custom_script']	= null;
      $data['content']	= "content/dashboard";

    $this->load->view('Main', $data);
  }

  //uang kas total
  function uangkas(){

      //$user aktifitas
      $user_aktif = $_SESSION['id_user'];
      $this->crud->set_table('uang_kas');
      $this->crud->set_subject('Tabel Transaksi Uang Kas');

      //colum yang ditampilkan
      $this->crud->columns('id_uangkas','saldo_kas','uang_masuk','uang_keluar','tanggal_transaksi','id_user');
      $this->crud->display_as('id_uangkas','Kode Transaksi');
      $this->crud->display_as('saldo_kas','Saldo');
      $this->crud->display_as('uang_masuk','Masuk');
      $this->crud->display_as('uang_keluar','Keluar');
      $this->crud->display_as('tanggal_transaksi','Tanggal Transaksi');

      //cek username
      $this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));

      //required_fields
      $this->crud->required_fields('saldo_kas','uang_masuk','uang_keluar','id_user');
      $this->crud->add_fields('saldo_kas','uang_masuk','uang_keluar','id_user');

      //api
      $this->crud->unset_edit();
          $this->crud->unset_print();

          $output = $this->crud->render();
          $output->gcrud = 1;
          $output->title = "Uang Kas Total BAI";
          $output->content = "content/blank";

      $this->load->view('grucery', $output);
  }

  //uang kas periode kepemimpinan
  function uangkasPeriode(){
  //$user aktifitas
  $user_aktif = $_SESSION['id_user'];
  $this->crud->set_table('uang_kas_periode');
  $this->crud->set_subject('Transaksi Uang Kas Periode');

  //colum yang ditampilkan
  $this->crud->columns('id_periode','uang_masuk','uang_keluar','saldo_kas','tgl_transaksi','id_user');
  $this->crud->display_as('id_periode','Kas Periode');
  $this->crud->display_as('saldo_kas','Saldo');
  $this->crud->display_as('uang_masuk','Masuk');
  $this->crud->display_as('uang_keluar','Keluar');
  $this->crud->display_as('tgl_transaksi','Tanggal Transaksi');
  $this->crud->display_as('id_user','Transaksi');

  //cek username
  $this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));

  //required_fields
  $this->crud->required_fields('saldo_kas','uang_masuk','uang_keluar','id_user');
  $this->crud->add_fields('saldo_kas','uang_masuk','uang_keluar','id_user');

  //api
  $this->crud->unset_edit();
      $this->crud->unset_print();

      $output = $this->crud->render();
      $output->gcrud = 1;
      $output->title = "Uang Kas Periode BAI";
      $output->content = "content/blank";

  $this->load->view('grucery', $output);
}

  //transfer uangkasPeriode
  function tranferUangKas(){

  }

}
