<!--
========================================================
This Website using original template from W3layouts.com
And redesign by Indra Kusuma
Author : Indra Kusuma
Website : www.indrakusuma.web.id
Email : indrakusuma.udinus@gmail.com
HAPPY CODING!
=========================================================
-->
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?=$title; ?> | BAI UDINUS</title>

		<!-- icon for content | Indra Kusuma-->
		<link rel="shortcut icon" href="<?=base_url();?>assets/images/favicon-bai.png">
	<!-- Meta tag for SEO Optimazation | Indra Kusuma -->
	<meta content='<?=$deskripsi;?>' name='description'/>
	<meta content='<?=$keywords;?>' name='keywords'/>
	<meta content='Indonesia' name='geo.placename'/>
	<meta content="BAI Matholi'ul Anwar"  name='Author'/>
	<meta content='general' name='rating'/>
	<!-- Link for External CSS | Indra Kusuma -->
	<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/css/animate.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/css/owl.carousel.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/css/smoothbox.css">
	<!-- CSS Custom for Speed | Indra Kusuma -->
	<?php
		if ($css_custom!=null) {
			$this->load->view("css_custom/$css_custom");
		}
	?>
	<link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">

	<!--js-->
	<!--webfonts-->
	<link href='https://fonts.googleapis.com/css?family=Josefin+Sans:400,700italic,700,600italic,600,400italic,300italic,300,100italic,100' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>
<body>
	<?php
		/* Header dan Menu | Indra Kusuma */
		$this->load->view('layout/header');
		/* Slider | Indra Kusuma */
		if ($slider!=null) {
			$this->load->view('layout/slide');
		}
		if ($breadcumb!=null) {
			$this->load->view('layout/breadcumb');
		}

		$this->load->view("content/$content");
		/*footer | Indra Kusuma*/
		$this->load->view('layout/footer');
	?>

	<!-- script for JS-->
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- Javascript | Indra Kusuma -->
	<script src="<?=base_url();?>assets/js/jquery.min.js"></script>
	<script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>assets/js/smoothbox.jquery2.js"></script>

	<script src="<?=base_url();?>assets/js/responsiveslides.min.js"></script>
	 <script>
			$(function () {
				$("#slider").responsiveSlides({
					auto: true,
					nav: true,
					speed: 500,
					namespace: "callbacks",
					pager: true,
				});
			});
		</script>
	<script src="<?=base_url();?>assets/js/owl.carousel.js"></script>
		<script>
			$(document).ready(function() {
			$("#owl-demo").owlCarousel({
				items : 1,
				lazyLoad : true,
				autoPlay : true,
				navigation : false,
				navigationText :  false,
				pagination : true,
			});
			});
		</script>

	<script type="text/javascript" src="<?=base_url();?>assets/js/wow.min.js"> </script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
				new WOW().init();
		});
		</script>
	</script>

	<?php if ($js_custom!=null) {
		$this->load->view("js_custom/$js_custom");
	} ?>
</body>
</html>
