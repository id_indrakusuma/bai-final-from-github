<?php 
    $id = $this->uri->segment(3);
    $detail = $this->Main_model->getDetailEvent($id);
    $tanggal = $detail['tanggal_kegiatan'];
?>

<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <section class="invoice">
      <!-- title row -->
      <div class="row">
      <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-calendar-check-o"></i> <?=$detail['nama_kegiatan'];?>
            <small class="pull-right">Acara ditulis oleh : <b><?=$detail['nama_user'];?></b></small>
          </h2>
        </div>
      </div>
      <!-- info row -->
      <div class="row invoice-info">
      <div class="col-sm-12 invoice-col">
          <address>
            Nama Kegiatan<br>
            <strong><i class="fa fa-calendar-check-o"></i> <?=$detail['nama_kegiatan'];?></strong><br>
            Waktu Pelaksanaan <br>
            <strong><i class="fa fa-clock-o"></i> <?=$detail['jam_mulai'];?>   <?=date("l, d F Y", strtotime($tanggal));?></strong><br>
            Tempat Kegiatan <br>
            <strong><i class="fa fa-map-marker"></i> <?=$detail['tempat'];?></strong><br>
            Jenis Kegiatan <br>
            <strong><i class="fa fa-info"></i> <?=$detail['jenis_kegiatan'];?></strong><br>
            Target Peserta <br>
            <strong><i class="fa fa-users"></i> <?=$detail['jenis_pe'];?></strong><br>
            Penanggung Jawab / Ketua Event <br>
            <strong><i class="fa fa-user"></i> <?=$detail['pj_kegiatan'];?></strong><br><br>
            <strong>Deskripsi Acara</strong> <br>
            " <?=$detail['deskripsi_kegiatan'];?> "<br>


          </address>
        </div>
      </div>

      <!-- this row will not appear when printing -->
      <div class="row no-print ">
        <div class="col-xs-12">
          
          <div class="pull-right">
            <a href="<?=base_url();?>admin/hapusEvent/<?=$detail['id_kegiatan'];?>" class="btn btn-sm btn-flat btn-danger"><i class="fa fa-eraser"></i> Hapus </a>
          <a href="<?=base_url();?>admin/ubahEvent/<?=$detail['id_kegiatan'];?>" class="btn btn-sm btn-flat btn-warning"><i class="fa fa-edit"></i> Ubah </a>
          <a href="#" class="btn btn-sm btn-flat btn-primary"><i class="fa fa-print"></i> Cetak </a>
            <a href="<?=base_url();?>admin/event" class="btn btn-sm btn-flat btn-success"><i class="fa fa-search"></i> Lihat Event Lainya </a>
          </div>
        </div>
      </div>
    </section>
  </section>
    <!-- /.content -->
</div>