<!--special-->
<div class="content">
				<div class="special-w3">
				<h3 class="tittle1 wow fadeInRight">Event BAI Matholi'ul Anwar</h3>
					<div class="container">
						<div class="special-grids">

						<?php
							$no = 0.2;
							foreach ($this->Main_model->getEvent() as $data) {
							$no = $no + 0.2;
							$tanggal = $data['tanggal_pelaksanaan'];
						?>
							<div class="col-md-4 special-grid wow fadeIn" data-wow-duration="1.5s" data-wow-delay="<?=$no;?>s">
								<div class="special1">
									<img src="<?=base_url(); ?>bai-admin/uploads/<?=$data['foto_event'];?>" class="img-responsive" alt=""/>
									<div class="special-icon hvr-sweep-to-top">
										<a href="<?=base_url();?>main/detailEvent/<?=$data['id_event'];?>"><i class="glyphicon glyphicon-th" aria-hidden="true"></i></a>
									</div>
								</div>
								<div class="special-bottom">
									<h4><?=$data['nama_event'];?></h4>
									<p>
										<i class="glyphicon glyphicon-calendar"></i> <?=date("l, d F Y", strtotime($tanggal));?><br>
										<i class="glyphicon glyphicon-user"></i> <?=$data['jenis_pe'];?><br>
										<i class="glyphicon glyphicon-map-marker"></i> <?=$data['lokasi_event'];?><br>
									</p>
								</div>
							</div>
						<?php }
						?>

							<div class="clearfix"></div>
						</div>
					</div>
				</div>
		</div>
			<!--special-->
