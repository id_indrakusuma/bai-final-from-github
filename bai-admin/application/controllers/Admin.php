<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!isset($_SESSION)){
            session_start();
        }
        if ($_SESSION['role_user'] != 'admin' && $_SESSION['role_user'] != 'superadmin') {
        	redirect(base_url());
        }

				//load library GRUCERY CRUD
        $this->load->library('grocery_CRUD');
        $this->crud = new grocery_CRUD();

	}
	public function index()
	{
		$data['title']		= "Dashboard";
		$data['custom_css']	= null;
		$data['custom_js']	= null;
		$data['custom_script']	= null;
    $data['content']	= "content/dashboard";

		$this->load->view('Main', $data);
	}

    /* FUNGSI CEK USER */

    /* SYSTEM LOG | Indra Kusuma */
	public function logout(){
        //-> system log
            $tabel = 'system_log';
            $data = array(
                    'id_user' => $_SESSION['id_user'],
                    'aktifitas' => 0 //-> 1 untuk login dan 0 untuk logout
                );
            $this->Main_model->tambah_data($tabel,$data);
        session_destroy();
        redirect(base_url());
    }
		/*
    //-> system log
    public function user_log()
    {
    	$data['title']		= "Aktifitas User";
    	$data['custom_css']	= "data_tables_css";
    	$data['custom_js']	= "data_tables_js";
    	$data['custom_script']	= "script_data_tables";
    	$data['content']	= "content/user_log";

    	$this->load->view('Main', $data);
    }
    /* FUNGSI USER | ADD | DELETE | UPDATE | Indra Kusuma*/
    //-> fungsi untuk menampilkan daftar_user yang aktif
		/*
		public function user()
    {
    	$data['title']		= "Pengguna BAI";
    	$data['custom_css']	= "data_tables_css";
    	$data['custom_js']	= "data_tables_js";
    	$data['custom_script']	= "script_data_tables";
    	$data['content']	= "content/user_bai";

    	$this->load->view('Main', $data);
    }

    public function addUser()
    {
        $data['title']      = "Penambahan User";
        $data['custom_css'] = null;
        $data['custom_js']  = null;
        $data['custom_script']  = null;
        $data['content']    = "crud/add_user";

        $this->load->view('Main', $data);
    }

    public function editUser()
    {

        $data['title']      = "Ubah Data User";
        $data['custom_css'] = null;
        $data['custom_js']  = null;
        $data['custom_script']  = null;
        $data['content']    = "crud/edit_user";

        $this->load->view('Main', $data);
    }

    public function tambahkanUser()
    {
        $tabel = 'user';
        $nama = $this->input->post('nama_user');
        $email = $this->input->post('email_user');
        $password = md5($this->input->post('password_user'));
        $jabatan = $this->input->post('role');

        $data = array(
            'nama_user' => $nama,
            'email_user' => $email,
            'password_user' => $password,
            'role' => $jabatan
            );


        $this->Main_model->tambah_data($tabel,$data);
            //-> Informasi
            $this->session->set_flashdata('info', 'Data berhasil diinputkan!');
        redirect("admin/user");
    }

    public function updateUser()
    {
        $tabel = 'user';
        $id = $this->input->post('id_user');
        $nama = $this->input->post('nama_user');
        $email = $this->input->post('email_user');
        $password = md5($this->input->post('password_user'));
        $jabatan = $this->input->post('role');

        $data = array(
            'id_user'   => $id,
            'nama_user' => $nama,
            'email_user' => $email,
            'password_user' => $password,
            'role' => $jabatan
            );

        $this->Main_model->updateUser($id,$data);
        redirect('Admin/user');
    }

    public function deleteUser()
    {
        $tabel = 'user'; //-> nama tabel yang akan dihapus
        $id = $this->uri->segment(3); //-> Uri routing yang diambil dari slsh ke tiga base_Url()/Admin/DeleteUser/uriSegment

        $this->Main_model->hapusUser($tabel, $id);
        $this->session->set_flashdata('info', 'Data berhasil dihapus!');
        redirect('Admin/user');
    }

    /* FUNGSI ARTIKEL | HAPUS | DELETE | UPDATE | Indra Kusuma */
     //-> fungsi untuk menampilkan daftar artikel

		 /*
    public function artikel()
    {
        $data['title']      = "Artikel";
        $data['custom_css'] = "data_tables_css";
        $data['custom_js']  = "data_tables_js";
        $data['custom_script']  = "script_data_tables";
        $data['content']    = "content/artikel";

        $this->load->view('Main', $data);
    }

    public function addArtikel()
    {
        $data['title']      = "Tambahkan Artikel";
        $data['custom_css'] = "artikel_css";
        $data['custom_js']  = "artikel_js";
        $data['custom_script']  = "artikel_script_js";
        $data['content']    = "crud/add_artikel";

        $this->load->view('Main', $data);
    }

        //->fungsi untuk menambahkan data dengan beda berbagai jenis file
        //-> referensi di http://fabernainggolan.net/upload-image-rename-codeigniter-dan-menyimpan-ke-database
        //-> atau di https://www.codeigniter.com/userguide3/libraries/file_uploading.html
    public function tambahkanArtikel()
    {
        $nama_file                      = "file_".time();
        $config['upload_path']          = './assets/uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2048;
        $config['file_name']            = $nama_file;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('gambar_artikel'))
        {
            echo "proses upload gagal!";
        }
        else
        {
            $gambar = $this->upload->data();//-> upload gambar ke file manager

            $tabel = 'artikel';
            $id = $this->input->post('id_user');
            $judul = $this->input->post('judul_artikel');
            $isi = $this->input->post('isi_artikel');
            $kategori = $this->input->post('id_kategori');

            $data = array(
                'id_user'   => $id,
                'judul_artikel' => $judul,
                'gambar_artikel' => $gambar['file_name'],
                'isi_artikel' => $isi,
                'id_kategori' => $kategori
                );

            $this->Main_model->tambah_data($tabel,$data);
            $this->session->set_flashdata('info', 'Artikel berhasil diterbitkan !');
            redirect('Admin/artikel');
        }

    }

    public function hapusArtikel()
    {
        $tabel = 'artikel'; //-> nama tabel yang akan dihapus
        $id = $this->uri->segment(3); //-> Uri routing yang diambil dari slsh ke tiga base_Url()/Admin/DeleteUser/uriSegment
        $this->Main_model->hapusArtikel($tabel, $id);
        $this->session->set_flashdata('info', 'Artikel berhasil dihapus !');
        redirect('Admin/artikel');
    }

    public function ubahArtikel()
    {

        $data['title']      = "Ubah Artikel";
        $data['custom_css'] = "artikel_css";
        $data['custom_js']  = "artikel_js";
        $data['custom_script']  = "artikel_script_js";

        $data['content']    = "crud/edit_artikel";

        $this->load->view('Main', $data);
    }

    public function updateArtikel()
    {
        $nama_file                      = "file_".time();
        $config['upload_path']          = './assets/uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2048;
        $config['file_name']            = $nama_file;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('gambar_artikel'))
        {
            echo "proses upload gagal!";
        }
        else
        {
            $gambar = $this->upload->data();//-> upload gambar ke file manager

            $tabel = 'artikel';
            $id_user = $this->input->post('id_user');
            $id = $this->input->post('id_artikel');
            $judul = $this->input->post('judul_artikel');
            $isi = $this->input->post('isi_artikel');
            $kategori = $this->input->post('id_kategori');

            $data = array(
                'id_user'   => $id_user,
                'judul_artikel' => $judul,
                'gambar_artikel' => $gambar['file_name'],
                'isi_artikel' => $isi,
                'id_kategori' => $kategori
                );

            $this->Main_model->updateArtikel($id,$data);
            redirect('Admin/artikel');
        }
    }

    public function addKategori()
    {

        $data['title']      = "Tambah Kategori Artikel";
        $data['custom_css'] = null;
        $data['custom_js']  = null;
        $data['custom_script']  = null;

        $data['content']    = "crud/add_kategori";

        $this->load->view('Main', $data);
    }

    public function tambahkanKategori()
    {
        $tabel = 'kategori_artikel';
        $new   = $this->input->post('judul_kategori');

        $data  = array(
            'judul_kategori' => $new
            );

        $this->Main_model->tambah_data($tabel, $data);
        $this->session->set_flashdata('info2', 'Kategori berhasil ditambahkan !');
        redirect('admin/artikel');
    }

    public function hapusKategori()
    {
        $id = $this->uri->segment(3); //-> Uri routing yang diambil dari slsh ke tiga base_Url()/Admin/DeleteUser/uriSegment
        $this->Main_model->hapusKategori($id);
        $this->session->set_flashdata('info2', 'Kategori berhasil dihapus !');
        redirect('Admin/artikel');
    }
    /* END OF FUNGSI ARTIKEL */

    /* FUNGSI event */

		/*
    function eventNow()
    {
        $data['title']      = "Kalender Kegiatan";
        $data['custom_css'] = "data_tables_css";
        $data['custom_js']  = "data_tables_js";
        $data['custom_script']  = "script_event";

        $data['content']    = "content/eventOn";

        $this->load->view('Main', $data);
    }

    function eventOff()
    {
        $data['title']      = "Kalender Kegiatan";
        $data['custom_css'] = "data_tables_css";
        $data['custom_js']  = "data_tables_js";
        $data['custom_script']  = "script_event";

        $data['content']    = "content/eventOff";

        $this->load->view('Main', $data);
    }

    function addEvent()
    {
        $data['title']      = "Tambah Kegiatan";
        $data['custom_css'] = "event_css";
        $data['custom_js']  = "event_js";
        $data['custom_script']  = "script_event2";

        $data['content']    = "crud/add_event";

        $this->load->view('Main', $data);
    }

    function ubahEvent()
    {
        $data['title']      = "Ubah Kegiatan";
        $data['custom_css'] = "event_css";
        $data['custom_js']  = "event_js";
        $data['custom_script']  = "script_event2";

        $data['content']    = "crud/edit_event";

        $this->load->view('Main', $data);
    }

    function tambahkanEvent()
    {
        $tabel          = 'kegiatan';
        $nm_kegiatan    = $this->input->post('nama_kegiatan');
        $tempat         = $this->input->post('tempat');
        $pj             = $this->input->post('pj_kegiatan');
        $desk           = $this->input->post('deskripsi_kegiatan');
        $tanggal        = $this->input->post('tanggal_kegiatan');
        $jam            = $this->input->post('jam_mulai');
        $id_user        = $this->input->post('id_user');
        $jenis          = $this->input->post('jenis_kegiatan');
        $peserta        = $this->input->post('peserta');

        $data = array(
            'nama_kegiatan' => $nm_kegiatan,
            'tempat' => $tempat,
            'pj_kegiatan' => $pj,
            'deskripsi_kegiatan' => $desk,
            'tanggal_kegiatan' => $tanggal,
            'jam_mulai' => $jam,
            'id_user' => $id_user,
            'id_jk' => $jenis,
            'id_pe' => $peserta
            );


        $this->Main_model->tambah_data($tabel,$data);
        $this->session->set_flashdata('info', 'Event berhasil ditambahkan !');
        redirect('Admin/event');
    }

    function updateEvent()
    {
        //-> id event
        $id             = $this->input->post('id_kegiatan');
        $nm_kegiatan    = $this->input->post('nama_kegiatan');
        $tempat         = $this->input->post('tempat');
        $pj             = $this->input->post('pj_kegiatan');
        $desk           = $this->input->post('deskripsi_kegiatan');
        $tanggal        = $this->input->post('tanggal_kegiatan');
        $jam            = $this->input->post('jam_mulai');
        $id_user        = $this->input->post('id_user');
        $jenis          = $this->input->post('jenis_kegiatan');
        $peserta        = $this->input->post('peserta');

        $data = array(
            'nama_kegiatan' => $nm_kegiatan,
            'tempat' => $tempat,
            'pj_kegiatan' => $pj,
            'deskripsi_kegiatan' => $desk,
            'tanggal_kegiatan' => $tanggal,
            'jam_mulai' => $jam,
            'id_user' => $id_user,
            'id_jk' => $jenis,
            'id_pe' => $peserta
            );

        $this->Main_model->updateEvent($id,$data);
        $this->session->set_flashdata('info', 'Event berhasil diperbarui !');
        redirect('Admin/event');
    }

    function HapusEvent()
    {
        $id = $this->uri->segment(3); //-> Uri routing yang diambil dari slsh ke tiga base_Url()/Admin/DeleteUser/uriSegment
        $this->Main_model->hapusEvent($id);
        $this->session->set_flashdata('info', 'Event berhasil dihapus !');
        redirect('Admin/event');
    }

    function detailEvent($id)
    {

        $data['title']      = "Detail Kegiatan";
        $data['custom_css'] = null;
        $data['custom_js']  = null;
        $data['custom_script']  = null;

        $data['content']    = "detail/detail_event";


        $this->load->view('Main', $data);
    }

		*/
    /*END of Event*/

		public function mutobaah(){ //untuk user
	    //user_aktif
	    $user_aktif = $_SESSION['id_user'];
	    $this->crud->set_table('mutobaah');
	    $this->crud->set_subject('Form Mutobaah');

	    $this->crud->columns('id_user','id_bid_mutobaah','tgl_mutobaah');
	    //display_as
	    $this->crud->display_as('id_bid_mutobaah','Bidang Mutobaah');
	    $this->crud->display_as('tgl_mutobaah','Tanggal Pengisian');
	    $this->crud->display_as('id_user','Nama');
	      //-> form pertanyaan mutobaah
	      $this->crud->display_as('s_limaWaktu','Hari ini Sholat Wajib 5 Waktu ?');
	      $this->crud->display_as('s_sunahRawatib','Hari ini Sholat Sunnah Rawatib ?');
	      $this->crud->display_as('s_qiyamulail','Hari ini Sholat Qiyamulail ?');
	      $this->crud->display_as('s_sunahDhuha','Hari ini Sholat Sunnah Dhuha ?');
	      $this->crud->display_as('puasaSunah','Hari ini Puasa Sunnah ?');
	      $this->crud->display_as('sedekah','Hari ini Bersedekah ?');
	      $this->crud->display_as('baca_quran','Hari ini Berapa kali membaca Alquran ? (Jawab Dengan Angka)');

	    //relasi ke tabel bidang mutobaah dan user
	    $this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));
	    $this->crud->set_relation('id_bid_mutobaah','mutobaah_bidang','nama_bidang');

	    //add
	    $this->crud->add_fields('id_user','id_bid_mutobaah','s_limaWaktu','s_sunahRawatib','s_qiyamulail','s_sunahDhuha','puasaSunah','sedekah','baca_quran');

			$this->crud->unset_edit();
	    $this->crud->unset_add();
	    $this->crud->unset_print();
	    $this->crud->unset_export();

	        $output = $this->crud->render();
	        $output->gcrud = 1;
	        $output->title = "Form Mutobaah Kaderisasi BAI";
	        $output->content = "content/blank";

	    $this->load->view('grucery', $output);
	  }

		public function calonAnggota(){
			$user_aktif = $_SESSION['id_user'];
	    $this->crud->set_table('calon_anggota');
	    $this->crud->set_subject('Form Pendaftaran BAI');

	    //display_as
			$this->crud->columns('nim','nama_lengkap','mentoring','status','id_user','tanggal_daftar');
	    $this->crud->display_as('nim','NIM');
	    $this->crud->display_as('nama_lengkap','Nama Lengkap');
			$this->crud->display_as('status','Status');
	    $this->crud->display_as('id_user','Diperiksa');
			$this->crud->display_as('tanggal_daftar','Registrasi');

			  $this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));
				$this->crud->edit_fields('nama_lengkap','nim','status','id_user');
				$this->crud->required_fields('id_user');

		    $this->crud->unset_add();
		    $this->crud->unset_print();

		        $output = $this->crud->render();
		        $output->gcrud = 1;
		        $output->title = "Hasil Pendaftaran Anggota BAI BAI";
		        $output->content = "content/blank";

		    $this->load->view('grucery', $output);

		}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */
