<!--featured-->
			<div class="featured-w3l">
				<div class="container">
					<h3 class="tittle1 wow fadeInLeft">Kegiatan Rutin</h3>
					<div class="feature-grids">

					<?php
					$no = 0.5;
					foreach ($this->Main_model->getKegiatanRutin() as $data){
						$no = $no + 0.2;
						?>
							<div class="col-md-4 fer-grid wow fadeIn" data-wow-duration="2s" data-wow-delay="<?=$no;?>s">
							<div class="icons">
								<i class="<?=$data['logo_icon'];?>" aria-hidden="true"></i>
							</div>
								<h4><?=$data['nama_kegiatan'];?></h4>
								<p><?=$data['deskripsi'];?>
									<br>
									Dilaksankan pada jam <b><?=$data['jam_kegiatan'];?></b> , Setiap Hari <b><?=$data['hari'];?></b>
								</p>
						</div>
						<?php
					}
					?>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		<!--featured-->
