<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Do your magic here
		if(!isset($_SESSION)){
            session_start();
        }
        if ($_SESSION['role_user'] != 'admin' && $_SESSION['role_user'] != 'superadmin') {
        	redirect(base_url());
        }
        //load library GRUCERY CRUD
        $this->load->library('grocery_CRUD');
        $this->crud = new grocery_CRUD();
  }

  function index()
  {
    redirect('Admin');
  }

  //user LOG
  public function userLog(){
    $this->crud->set_table('system_log');
    $this->crud->set_subject('Aktifitas User BAI');

    //set columns
    $this->crud->columns('id_user','aktifitas','tanggal_log');

    //display_as
    $this->crud->display_as('id_user','Nama Pengguna');
    $this->crud->display_as('aktifitas','Aktifitas');
    $this->crud->display_as('tanggal_log','Tanggal');
    //relation
    $this->crud->set_relation('id_user','user','nama_user');
    $this->crud->callback_column('aktifitas',array($this, '_callback_aktifitas'));

    //GRUCERY application
    $this->crud->unset_print();
    $this->crud->unset_add();
    $this->crud->unset_edit();
    $this->crud->unset_read();

    $output = $this->crud->render();
    $output->gcrud = 1;
    $output->title = "Aktifitas Pengguna UKM BAI";
    $output->content = "content/blank";
    $this->load->view('grucery', $output);


  }

  //function callback merubah 0 & 1 menjadi string
  public function _callback_aktifitas($value, $row){
    $tmp = "";
    $val = $row->aktifitas;
    if ($val == "1") {
      $tmp = "User Melakukan Login";
    }else{
      $tmp = "User Logout";
    }
    return $tmp;
  }
  //function user

  public function user()
  {

    $user_aktif = $_SESSION['id_user'];
    $this->crud->set_table('user');
    $this->crud->set_subject('User BAI');

    //colum pada table
    $this->crud->columns('id_user','nama_user','foto_user','email_user','role');
    //merubah nama field sesuai dengan keinginan
    $this->crud->display_as('id_user', 'Kode User');
    $this->crud->display_as('nama_user','Nama Pengguna');
    $this->crud->display_as('email_user','Email Pengguna');
    $this->crud->display_as('foto_user','Foto');
    $this->crud->display_as('role','Role');

    //form wajib
    $this->crud->required_fields('nama_user','email_user','role','password_user');

    //
    //validasi gambar
    $this->crud->set_field_upload('foto_user', 'uploads');

    $this->crud->callback_before_upload(array($this, '_valid_images'));
    //This Callback Encryption
    //$this->crud->callback_before_insert(array($this,'encrypt_password_callback'));
    $this->crud->callback_before_update(array($this,'encrypt_password_callback'));
    $this->crud->callback_edit_field('password_user',array($this,'set_userpassword_input_to_empty'));


    //grucery API
    $this->crud->unset_delete();
    $this->crud->unset_read();
        $this->crud->unset_export();
        $this->crud->unset_print();

    $output = $this->crud->render();
    $output->gcrud = 1;
    $output->title = "USER UKM BAI";
    $output->content = "content/blank";
    $this->load->view('grucery', $output);
  }

  //fungsi untuk upload gambar
  public function _valid_images($files_to_upload, $field_info)
  {
    if ($files_to_upload[$field_info->encrypted_field_name]['type'] != ('image/png' || 'image/jpg' || 'image/jpeg'))
    {
      return 'Maaf ! Gambar yang diperbolehkan hanya bertipe PNG / JPG / JPEG';
    }
    return true;
  }

  //callback untuk password
  function encrypt_password_callback($post_array) {
        $post_array['password_user'] = md5($post_array['password_user']);
        return $post_array;
    }

    function set_userpassword_input_to_empty() {
        return "<input type='password' class='form-control' name='password_user' value='' />";
    }



}
