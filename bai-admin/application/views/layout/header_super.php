<header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url();?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>B</b>AI</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>BAI</b> Online System</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php //cek foto_user
                if ($_SESSION['foto_user'] == "") {
                  ?>
                    <img src="<?=base_url();?>assets/img/not_ready.png"  class="user-image" alt="User Image">
                  <?php
                }else{
                  ?>
                  <img src="<?=base_url();?>uploads/<?=$_SESSION['foto_user'];?>" class="user-image" alt="User Image">
                  <?php
                }
              ?>
              <span class="hidden-xs">Hai, <?=$_SESSION['nama_user']; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <?php
                  //cek foto_user
                  if ($_SESSION['foto_user'] == "") {
                    ?>
                    <img src="<?=base_url();?>assets/img/not_ready.png" class="img-circle" alt="User Image">
                    <?php
                  }else{
                    ?>
                    <img src="<?=base_url();?>uploads/<?=$_SESSION['foto_user'];?>" class="img-circle" alt="User Image">
                    <?php
                  }
                ?>

                <p>
                  <?=$_SESSION['nama_user']; ?>
                  <small>Hai, <?=$_SESSION['role_user']; ?>!</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">

                </div>
                <div class="pull-right">
                  <a href="<?=base_url();?>Admin/logout" class="btn btn-danger btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
