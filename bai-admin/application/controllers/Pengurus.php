<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengurus extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!isset($_SESSION)){
    		session_start();
		}
        if ($_SESSION['role_user'] != 'pengurus' && $_SESSION['role_user'] != 'superadmin') {
        	redirect(base_url());
        }
        //load library GRUCERY CRUD
        $this->load->library('grocery_CRUD');
        $this->crud = new grocery_CRUD();
	}

	public function index()
	{
		$data['title']		= "Dashboard";
		$data['custom_css']	= null;
		$data['custom_js']	= null;
		$data['custom_script']	= null;
    	$data['content']	= "content/dashboard";

		$this->load->view('Main', $data);
	}
	public function logout(){
        //-> system log
            $tabel = 'system_log';
            $user = $_SESSION['id_user'];
            $data = array(
                    'id_user' => $user,
                    'aktifitas' => 0 //-> 1 untuk login dan 0 untuk logout
                );
            $this->Main_model->tambah_data($tabel,$data);
        session_destroy();
        redirect(base_url());
    }

    public function mutobaah(){ //untuk user
	    $this->crud->set_table('mutobaah');
	    $this->crud->set_subject('Form Mutobaah');

	    $this->crud->columns('id_user','id_bid_mutobaah','tgl_mutobaah');
	    //display_as
	    $this->crud->display_as('id_bid_mutobaah','Bidang Mutobaah');
	    $this->crud->display_as('tgl_mutobaah','Tanggal Pengisian');
	    $this->crud->display_as('id_user','Nama');
	      //-> form pertanyaan mutobaah
	      $this->crud->display_as('s_limaWaktu','Hari ini Sholat Wajib 5 Waktu ?');
	      $this->crud->display_as('s_sunahRawatib','Hari ini Sholat Sunnah Rawatib ?');
	      $this->crud->display_as('s_qiyamulail','Hari ini Sholat Qiyamulail ?');
	      $this->crud->display_as('s_sunahDhuha','Hari ini Sholat Sunnah Dhuha ?');
	      $this->crud->display_as('puasaSunah','Hari ini Puasa Sunnah ?');
	      $this->crud->display_as('sedekah','Hari ini Bersedekah ?');
	      $this->crud->display_as('baca_quran','Hari ini Berapa kali membaca Alquran ? (Jawab Dengan Angka)');

	    //relasi ke tabel bidang mutobaah dan user
	    $this->crud->set_relation('id_user','user','nama_user');
	    $this->crud->set_relation('id_bid_mutobaah','mutobaah_bidang','nama_bidang');

		$this->crud->unset_edit();
	    $this->crud->unset_add();
	    $this->crud->unset_print();
	    $this->crud->unset_export();
	    $this->crud->unset_delete();

	        $output = $this->crud->render();
	        $output->gcrud = 1;
	        $output->title = "Form Mutobaah Kaderisasi BAI";
	        $output->content = "content/blank";

	    $this->load->view('grucery', $output);
	  }
}

/* End of file Pengurus.php */
/* Location: ./application/controllers/Pengurus.php */
