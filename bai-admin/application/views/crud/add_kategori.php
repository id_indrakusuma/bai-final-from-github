<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small>BAI Online System</small>
      </h1>
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="<?=base_url();?>admin/tambahkanKategori" method="post">
                <!-- text input -->
                <div class="form-group">
                  <label>Kategori Yang Sudah Tersedia</label>
                  <select class="form-control" name="id_kategori">
                    <?php foreach ($this->Main_model->getKategoriArtikel() as $kat) {
                      ?>
                        <option value="<?=$kat['id_kategori'] ;?>"> <?=$kat['judul_kategori'];?></option>
                      <?php
                    } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Kategori Baru</label>
                  <input type="text" name="judul_kategori" class="form-control" placeholder="Tambahkan kategori baru.." required="">
                </div>
                <div>
                  <input type="submit" class="btn btn-md btn-primary" value="Tambahkan">
                  <input type="reset" class="btn btn-md btn-warning" value="Reset">
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
    </section>
    <!-- /.content -->
  </div>