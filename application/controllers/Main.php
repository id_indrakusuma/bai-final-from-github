<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
		$this->load->helper('url');
	}

	public function index()
	{
		$data['title'] 		= "Selamat Datang di Website Resmi BAI Matholi'ul Anwar";
		$data['deskripsi'] 	= "UKM Badan Amalan Islam (BAI) Matholi’ul Anwar telah berdiri sejak tanggal 7 Oktober 2001 dengan tujuan menjadi pusat pendidikan ilmu agama islam yang menyeluruh (Kaffah) bukan dalam lingkungan mahasiswa saja namun juga dalam lingkungan masyarakat yang juga terlibat didalamnya.";
		$data['keywords'] 	= "BAI, bai udinus, udinus bai, badan amalan islam udinus, udinus bai, bai udinus";
		//-> Addon | jika tidak diperlukan silahkan isikan null
		$data['slider'] 	= 1; //-> menampilkan slider, jika tidak diperlukan isikan dengan null
		$data['css_custom'] = 'slider';
		$data['js_custom']	= 1;
		$data['sambutan']   = 1;
		$data['why']		= 1;
		$data['layanan']	= 1;

		$this->load->view('Main',$data);
	}

	public function contact()
	{
		$data['title'] 		= "Contact Us ";
		$data['deskripsi'] 	= "Hubungi Kami untuk Informasi lebih Lanjut mengenai UKM Badan Amalan Islam UDINUS";
		$data['keywords'] 	= "BAI, bai udinus, udinus bai, badan amalan islam udinus, udinus bai, bai udinus";
		//-> Addon | jika tidak diperlukan silahkan isikan null
		$data['slider'] 	= null; //-> menampilkan slider, jika tidak diperlukan isikan dengan null
		$data['css_custom'] = 'slider';
		$data['js_custom']	= null;
		$data['breadcumb']	= "Contact";
		$data['content']	= "contact_us";

		$this->load->view('Content', $data);
	}

	public function galeri()
	{
		$data['title'] 		= "Galeri Kegiatan BAI";
		$data['deskripsi'] 	= "Berikut merupakan daftar foto kegiatan Badan Amalan Islam UDINUS";
		$data['keywords'] 	= "BAI, bai udinus, udinus bai, badan amalan islam udinus, udinus bai, bai udinus";
		//-> Addon | jika tidak diperlukan silahkan isikan null
		$data['slider'] 	= null; //-> menampilkan slider, jika tidak diperlukan isikan dengan null
		$data['css_custom'] = null;
		$data['js_custom']	= null;
		$data['breadcumb']	= "Gallery";
		$data['content']	= "galeri";

		$this->load->view('Content', $data);
	}

	function contact_add()
	{
		$tabel = 'contact_us';
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$telepon = $this->input->post('telepon');
		$pesan = $this->input->post('pesan');

		$data = array(
			'nama' => $nama,
			'email' => $email,
			'telepon' => $telepon,
			'pesan' => $pesan
			);

		$this->Main_model->tambah_data($tabel,$data);
		$this->session->set_flashdata('info', 'Terima Kasih atas Feedback Maupun Pertanyaan Anda ! Kami akan menjawabnya segera !');
		redirect('contact','refresh');
	}

	//add tambah anggota
	function mendaftar(){
		$tabel = 'calon_anggota';
		$nama_lengkap = $this->input->post('nama_lengkap');
		$nim = $this->input->post('nim');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$no_hp = $this->input->post('no_hp');
		$line_bbm = $this->input->post('line_bbm');
		$mentoring = $this->input->post('mentoring');

		$data = array(
			'nama_lengkap' => $nama_lengkap,
			'nim' => $nim,
			'jenis_kelamin' => $jenis_kelamin,
			'no_hp' => $no_hp,
			'line_bbm' => $line_bbm,
			'mentoring' => $mentoring
		);

		$this->Main_model->tambah_data($tabel,$data);

		$this->session->set_flashdata('info', 'Terimakasih! Anda telah berhasil mendaftar sebagai Anggota BAI, Tunggu Kabar dari Kami & Pastikan Nomor Kamu aktif ya !');
		redirect('daftar','refresh');
	}
	//pendaftaran anggota
	public function daftarMember(){
		$data['title'] 		= "Pendaftaran Anggota Baru BAI";
		$data['deskripsi'] 	= "Beikut merupakan formulir untuk menjadi anggota Badan Amalan Islam Matholi'ul Anwar UDINUS";
		$data['keywords'] 	= "daftar bai,gabung bai,BAI, bai udinus, udinus bai, badan amalan islam udinus, udinus bai, bai udinus";
		//-> Addon | jika tidak diperlukan silahkan isikan null
		$data['slider'] 	= null; //-> menampilkan slider, jika tidak diperlukan isikan dengan null
		$data['css_custom'] = 'slider';
		$data['js_custom']	= 'daftar';
		$data['breadcumb']	= "Daftar Sebagai Member";
		$data['content']	= "daftarMember";

		$this->load->view('Content', $data);
	}


	//_----------EROR 404
	public function eror404()
	{
		$data['title'] 		= "Eror 404 !";
		$data['deskripsi'] 	= "Eror 404! Mohon maaf, halaman yang Anda minta tidak kami temukan, silahkan periksa kembali link Anda";
		$data['keywords'] 	= "BAI, bai udinus, udinus bai, badan amalan islam udinus, udinus bai, bai udinus";
		//-> Addon | jika tidak diperlukan silahkan isikan null
		$data['slider'] 	= null; //-> menampilkan slider, jika tidak diperlukan isikan dengan null
		$data['css_custom'] = null;
		$data['js_custom']	= null;
		$data['breadcumb']	= 0;
		$data['content']	= "404";

		$this->load->view('Content', $data);
	}

	//_----------EVENT
	public function event()
	{
		$data['title'] 		= "Event";
		$data['deskripsi'] 	= "Berikut merupakan daftar Event lengkap yang akan dilaksanakan oleh Badan Amalan Islam UDINUS";
		$data['keywords'] 	= "BAI, bai udinus, udinus bai, badan amalan islam udinus, udinus bai, bai udinus";
		//-> Addon | jika tidak diperlukan silahkan isikan null
		$data['slider'] 	= null; //-> menampilkan slider, jika tidak diperlukan isikan dengan null
		$data['css_custom'] = null;
		$data['js_custom']	= null;
		$data['breadcumb']	= "Event";
		$data['content']	= "event";

		$this->load->view('Content', $data);
	}

	public function detailEvent()
	{
		$data['title'] 		= "Event";
		$data['deskripsi'] 	= "Berikut merupakan informasi lengkap mengenai Event Badan Amalan Islam UDINUS";
		$data['keywords'] 	= "BAI, bai udinus, udinus bai, badan amalan islam udinus, udinus bai, bai udinus";
		//-> Addon | jika tidak diperlukan silahkan isikan null
		$data['slider'] 	= null; //-> menampilkan slider, jika tidak diperlukan isikan dengan null
		$data['css_custom'] = null;
		$data['js_custom']	= null;
		$data['breadcumb']	= "Event";
		$data['content']	= "event_detail";

		$this->load->view('Content', $data);
	}

	public function tentang()
	{
		$data['title'] 		= "Tentang Kami";
		$data['deskripsi'] 	= "UKM Badan Amalan Islam (BAI) Matholi’ul Anwar telah berdiri sejak tanggal 7 Oktober 2001 dengan tujuan menjadi pusat pendidikan ilmu agama islam yang menyeluruh (Kaffah) bukan dalam lingkungan mahasiswa saja namun juga dalam lingkungan masyarakat yang juga terlibat didalamnya.";
		$data['keywords'] 	= "BAI, bai udinus, udinus bai, badan amalan islam udinus, udinus bai, bai udinus";
		//-> Addon | jika tidak diperlukan silahkan isikan null
		$data['slider'] 	= null; //-> menampilkan slider, jika tidak diperlukan isikan dengan null
		$data['css_custom'] = null;
		$data['js_custom']	= null;
		$data['breadcumb']	= "About";
		$data['content']	= "about";

		$this->load->view('Content', $data);
	}

	public function blog()
	{
		$data['title'] 		= "Blog";
		$data['deskripsi'] 	= "Berikut merupakan daftar Artikel yang telah dipublikasikan oleh Badan Amalan Islam Universitas Dian Nuswantoro Semarang";
		$data['keywords'] 	= "BAI, bai udinus, udinus bai, badan amalan islam udinus, udinus bai, bai udinus";
		//-> Addon | jika tidak diperlukan silahkan isikan null
		$data['slider'] 	= null; //-> menampilkan slider, jika tidak diperlukan isikan dengan null
		$data['css_custom'] = null;
		$data['js_custom']	= null;
		$data['breadcumb']	= "Artikel";
		$data['content']	= "blog";

		$this->load->view('Content', $data);
	}
	public function detailArtikel()
	{
		$data['title'] 		= "Blog";
		$data['deskripsi'] 	= "Berikut merupakan detail lengkap artikel yang telah diupbilikasian.";
		$data['keywords'] 	= "BAI, bai udinus, udinus bai, badan amalan islam udinus, udinus bai, bai udinus";
		//-> Addon | jika tidak diperlukan silahkan isikan null
		$data['slider'] 	= null; //-> menampilkan slider, jika tidak diperlukan isikan dengan null
		$data['css_custom'] = null;
		$data['js_custom']	= null;
		$data['breadcumb']	= "Artikel";
		$data['content']	= "blog_detail";

		$this->load->view('Content', $data);
	}

}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */
