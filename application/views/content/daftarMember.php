<!--whychoose-->
<div class="why-w3">
	<div class="container">
        <!-- form pendaftaran -->
        <div class="col-md-8 wow fadeInLeft">
          <div class="panel panel-success">
  						<div class="panel-heading">
  						  <h3 class="panel-title">Pendaftaran Anggota BAI 2017</h3>
  						</div>
  						<div class="panel-body">
  						  <!-- isi form -->
                <?php
                  $info = $this->session->flashdata('info');
                  if (isset($info)) {
                    ?>
                      <div class="alert alert-info wow bounceIn" role="alert" data-wow-duration="4s">
                        <h4 style="font-size:16px;"><strong> <?=$info;?> </strong></h4>
                      </div>
                    <?php
                  }
                ?>

                <form class="form-horizontal" method="post" action="<?=base_url();?>mendaftar" autocomplete="off">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="namaLengkap">Nama Lengkap</label>
                        <input type="text" class="form-control" id="namaLengkap" name="nama_lengkap" placeholder="Nama Lengkap" required="">
                    </div>
                    <div class="form-group">
                      <label for="nim">Nomor Induk Mahasiswa (NIM)</label>
                        <input type="text" class="form-control" id="nim" name="nim" placeholder="Misal : A11.2014.08316" required="">
												<span id="result"></span>
										</div>
                    <div class="form-group">
                      <label for="kelamin">Jenis Kelamin</label>
                        <select class="form-control" name="jenis_kelamin" id="kelamin">
                          <option value="L">Laki-Laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="no_hp">Nomor HP (Masih Aktif)</label>
                        <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="Misal : 089522248642" maxlength="15" required="">
                    </div>
                    <div class="form-group">
                      <label for="line_bbm">LINE/BBM</label>
                        <input type="text" class="form-control" id="line_bbm" name="line_bbm" placeholder="@id.indrakusuma">
                    </div>
                    <div class="form-group">
                      <label for="mentoring">Sudah Mengikuti Mentoring BAI ?</label>
                        <select class="form-control" name="mentoring" id="mentoring">
                          <option value="Belum">Belum</option>
                            <option value="Sudah">Sudah</option>
                        </select>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12">
                        <div class="checkbox pull-left">
                          <label>
                            <input type="checkbox" required=""> Data yang saya masukan sudah benar
                          </label>
                        </div>
                        <div class="col-sm-6 pull-right">
                          <button type="submit" class="btn btn-success pull-right" style="margin-right:-20px;">Daftar Sekarang</button>
                          <button type="reset" class="btn btn-danger pull-right" style="margin-right:10px;">Batal</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
  						</div>
  					  </div>
        </div>

            <?php foreach ($this->Main_model->getContactFull() as $data){
              ?>
              <div class="col-md-4 wow fadeInRight">
                <hr class="divider">
            <h3>More Info</h3>
              <ul>
                <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> Office : <?=$data['no_telp'];?></li>
                <li><i class="glyphicon glyphicon-phone" aria-hidden="true"></i> Mobile : <?=$data['no_hp'];?></li>
                <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i> <a href="#"><a href="mailto:<?=$data['email'];?>"><?=$data['email'];?></a></a></li>
                <li><i class="glyphicon glyphicon-print" aria-hidden="true"></i> Fax : <?=$data['fax'];?></li>
                <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i><?=$data['alamat'];?></li>
              </ul>
              <hr class="divider">
            </div>
              <?php
            } ?>

			<div class="clearfix"></div>
	</div>
</div>
