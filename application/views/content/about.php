<!--whychoose-->
<div class="why-w3">
	<div class="container">
		<div class="why-grids">
			<div class="col-md-6 why-grid wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.5s">
				<h3 class="tittle2">Visi & Misi</h3>
				<p style="text-align: justify;"><img src="<?=base_url();?>assets/images/logo_bai.png" style="max-height:12em; float:left; margin-right:20px; margin-left: 10px;">
				" Membina generasi muda Islam yang berilmu, beriman, dan bertaqwa kepada Allah Subhanahu Wa Taala serta senantiasa berjuang untuk menegakkan dan menyiarkan ajaran Islam di lingkungan kampus Universitas Dian Nuswantoro Semarang pada khususnya dan masyarakat pada umumnya. "<br><br><br></p>
				<p class="why-text">
					<ol style="color:#777; text-align: justify;">
						<li>Membina mahasiswa Islam Universitas Dian Nuswantoro Semarang sesuai syariat Islam.</li>
						<li>Berperan serta secara aktif dalam pengamalan syariat Islam di Universitas Dian Nuswantoro Semarang pada khususnya dan masyarakat pada umumnya. 3. Mempersiapkan kader-kader intelektual Muslim sebagai penerus perjuangan dakwah Islam.</li>
					</ol>
				</p>
			</div>

			<div class="col-md-6 why-grid wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.5s">
				<h3 class="tittle2">Sejarah Organisasi</h3>
				<p style="text-align: justify;">UKM BAI Matholi’ul Anwar tersebut telah berdiri sejak tanggal 7 Oktober 2001 dengan tujuan menjadi pusat pendidikan ilmu agama islam yang menyeluruh (Kaffah) bukan dalam lingkungan mahasiswa saja namun juga dalam lingkungan masyarakat yang juga terlibat didalamnya.<br>
					Dalam kemajuan IPTEK yang semakin pesat sekarang ini membawa dampak positif dan negatif yang sangat mempengaruhi berbagai kalangan masyarakat tidak terkecuali mahasiswa khususnya di UDINUS. Bila kemajuan tersebut tanpa dibatasi dengan filter maka tak ayal pengaruh negatif akan terserap. Dimana salah satu filter yang ampuh adalah dengan melakukan pendekatan keagamaan, sementara mahasiswa punya tanggung jawab moral untuk dirinya sebagai bagian dari elite masyarakat dan masyarakat menciptakan filter yang kuat atas pengaruh-pengaruh yang dapat merugikan. Maka untuk mengantisipasi hal tersebut, Universitas Dian Nuswantoro menciptakan suatu wadah lembaga UKM yang bergerak di bidang kerohanian yang diberi nama BAI (Badan Amalan Islam) Matholiul Anwar. Maksud dari Matholiul Anwar adalah tempat terbitnya cahaya, pendefinisian dalam bahasa arab sebagai upaya pembebasan umat dari kebodohan dan keterbelakangan melalui pendidikan dalam dakwah sebagai usaha perjuangan organisasi.
				</p>
			</div>

			<div class="clearfix"></div>
		</div>
	</div>
</div>
