<div class="footer-section">
						<div class="container">
							<div class="footer-grids">
								<div class="col-md-6 footer-grid wow fadeInLeft">
									<h4>Tentang BAI </h4>
									<p style="text-align: justify;"><img src="<?=base_url();?>assets/images/logo_bai.png" style="max-height:7.5em; float:left; margin-right:10px;"> UKM Badan Amalan Islam (BAI) Matholi’ul Anwar telah berdiri sejak tanggal 7 Oktober 2001 dengan tujuan menjadi pusat pendidikan ilmu agama islam yang menyeluruh (Kaffah) bukan dalam lingkungan mahasiswa saja namun juga dalam lingkungan masyarakat yang juga terlibat didalamnya.</p>
								</div>
								<div class="col-md-3 footer-grid wow fadeInRight">
									<h4>Link Penting</h4>
									<ul>
										<li><a href="http://dinus.ac.id">Universitas Dian Nuswantoro</a></li>
										<li><a href="http://bima.dinus.ac.id">Biro Kemahasiswaan UDINUS</a></li>
										<li><a href="http://indrakusuma.web.id">Islamic Fair</a></li>
										<li><a href="<?=base_url();?>daftar">Gabung BAI</a></li>
									</ul>
								</div>
								<div class="col-md-3 footer-grid wow fadeInRight">
									<h4>Hubungi Kami</h4>
									<?php
										foreach ($this->Main_model->getContactMin() as $data){
									?>
									<ul>
										<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i><?=$data['no_hp'];?></li>
										<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:<?=$data['email'];?>"><?=$data['email'];?></a></li>
										<li><i class="glyphicon glyphicon-time" aria-hidden="true"></i>Senin - Jum'at, 08.00 - 17.00 WIB</li>
										<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i><?=$data['alamat'];?></li>

									</ul>
								</div>
								<?php
								} ?>
							<div class="clearfix"> </div>
							</div>

						</div>
					</div>
					<!--footer-->
					<!--copy-->
					<div class="copy-section">
						<div class="container wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.5s">
							<p>&copy; 2016 Badan Amalan Islam Matholi'ul Anwar. All rights reserved | Powered by <a href="http://www.indrakusuma.web.id">BAI Matholi'ul Anwar</a></p>
						</div>
					</div>
		<!--Register-->
