<?php
    $isu = $this->db->query('SELECT * FROM user WHERE status_user = "1"');
    $artikel = $this->db->query('SELECT id_artikel FROM artikel');
    $inventaris = $this->db->query('SELECT id_inventaris FROM inventaris');
    $kegiatan_rutin = $this->db->query('SELECT id_kegitan_rutin FROM kegiatan_rutin');
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Beranda
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Beranda</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $artikel->num_rows(); ?></h3>

              <p>Artikel Terpublikasi</p>
            </div>
            <div class="icon">
              <i class="ion ion-compose"></i>
            </div>
            <a href="<?=base_url();?>grud/artikel" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $inventaris->num_rows(); ?></h3>

              <p>Total Inventaris</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-filing"></i>
            </div>
            <a href="<?=base_url();?>sekretaris/inventaris" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $isu->num_rows(); ?></h3>

              <p>Total User Aktif</p>
            </div>
            <div class="icon">
              <i class="ion ion-person"></i>
            </div>
            <a href="<?=base_url();?>Admin/user" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $kegiatan_rutin->num_rows(); ?></h3>

              <p>Kegitan Rutin</p>
            </div>
            <div class="icon">
              <i class="ion ion-calendar"></i>
            </div>
            <a href="#" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->
  </div>
