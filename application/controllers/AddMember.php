<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AddMember extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model('Main_model');
		$this->load->helper('url');
  }

  //memeriksa ketersedian NIM
  public function cek_nim(){
    $nim = $this->input->post('nim');
    $tersedia = $this->Main_model->cek_nim($nim);

    $count = count($tersedia);
    // echo $count

    if (empty($count)) {
        echo "<span style='color:green;'>NIM masih tersedia</span>";
        //return true;
    } else {
        echo "<span style='color:brown;'>Mohon maaf ! NIM Anda sudah terdaftar !</span>";
        //return false;
    }
  }

}
