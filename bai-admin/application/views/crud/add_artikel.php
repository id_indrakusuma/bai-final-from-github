  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small>BAI Online System</small>
      </h1>
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-md-10">
            <!--/.col (right) -->
        <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body pad">
                <?php echo form_open_multipart('Admin/tambahkanArtikel');?>
                  <div class="form-group">
                    <!-- input user | hidden -->
                    <input type="hidden" name="id_user" class="form-control" value="<?=$_SESSION['id_user'];?>">
                    <label>Judul Artikel</label>
                    <input type="text" name="judul_artikel" class="form-control" placeholder="Judul Artikel.." required="">
                  </div>
                  <div class="form-group">
                    <label>Gambar Artikel</label>
                    <input type="file" name="gambar_artikel">
                  </div>
                  <div class="form-group">
                    <textarea class="textarea" name="isi_artikel" placeholder="Tulis isi artikel disini.." style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                  
                  <div class="form-group">
                  <label>Kategori Artikel</label>
                  <select class="form-control select2" style="width: 100%;" name="id_kategori">
                    <?php foreach ($this->Main_model->getKategoriArtikel() as $kat) {
                      ?>
                        <option value="<?=$kat['id_kategori'] ;?>"> <?=$kat['judul_kategori'];?></option>
                      <?php
                    } ?>
                  </select>
                </div>
                  <div>
                    <input type="submit" class="btn btn-md btn-primary" value="Tambahkan Artikel">
                    <input type="reset" class="btn btn-md btn-warning" value="Reset">
                  </div>
                </form>
            </div>
          </div>

          </div>
      </div>
    </section>
    <!-- /.content -->
  </div>