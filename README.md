# IT Managemen System UKM BAI
Merupakan web aplikasi yang memiliki fungsi untuk managemen UKM badan Amalan Islam Matholi'ul Anwar.
**[Demo Aplikasi]** : _http://bai.dinus.ac.id_ atau di _http://ukmbai.zeintech.com_ 

##Sekilas Mengenai Sistem
Aplikasi ini memiliki 2 fungsi utama, yaitu :
1. Untuk sarana publikasi Kegiatan, Artikel, dan Pendaftaran Anggota Baru BAI.
2. Untuk Managemen Organisasi (pencatatan surat, lembar mutobaah, informasi keuangan, pengolahan event, evaluasi event dan segala kegiatan lainya)

##Bug Saat Ini
- Fungsi Keuangan & kas belum benar

##Sistem Ini Ditunjukan Untuk
- UKM Badan Amalan Islam UDINUS
- Pemrograman Internet (Dosen : Pak Ajib Susanto, M.kom)
- Tugas Managemen Proyek (Bu. Novita)
- Tugas Sistem Basis Data
- Tugas Rekayasa Perangkat Lunak (Bu Ayu Pertiwi, S.Kom, M.T)

##Kedepan
- Membuat dokumentasi (Sementara SKPL & DPPL udah 80%) 
- User Manual & Guide

##Thanks to
- Allah SWT
- Bapak & Ibu 
- UKM BAI
- my best patner @mnirfan (M Nurul Irfan) & @zaanfa (Zakiy Anwar Fakhri)

##Lisensi
Untuk hak cipta dari aplikasi ini dipegang sepenuhnya oleh **Indra Kusuma** dan **UKM BAI**
