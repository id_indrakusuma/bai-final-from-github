 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.2 (Beta Version)
    </div>
    <strong>Copyright &copy; 2016 <a href="http://www.indrakusuma.web.id">Indra Kusuma</a>.</strong> All rights
    reserved.
  </footer>
