<!--Testimonial-->
			<div class="testimonials-w3l">
				<div class="container">
					<h3 class="tittle1 wow fadeInRight">Sambutan</h3>
					<div class="testimonial-grids">

					<?php
						$no = 0;
						foreach ($this->Main_model->getSambutan() as $data){
							$no = $no + 0.5;
						?>
						<div class="col-md-6 test-grid wow fadeIn" data-wow-duration="2s" data-wow-delay="<?=$no;?>s">
							<div class="col-md-4 test-left">
								<img src="<?=base_url();?>bai-admin/uploads/<?=$data['foto_sambutan'];?>" class="img-responsive" alt=""/>
							</div>
							<div class="col-md-8 test-right">
								<p><?=$data['isi_sambutan'];?></p>
								<h5><?=$data['nama_ketua'];?>, <?=$data['jabatan'];?></h5>
							</div>
							<div class="clearfix"></div>
						</div>
						<?php
						}
					?>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		<!--Testimonial-->
