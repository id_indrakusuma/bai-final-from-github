<script>
  $(function () {
    //Datatables
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });

    $(".textarea").wysihtml5();
  });
</script>