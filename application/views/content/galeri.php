<!--Projects-->
		<div class="content">
			<div class="projects-agile">
				<div class="container">
					<h2 class="tittle wow fadeInRight"><?=$title;?></h2>
						<div class="portfolio_grid_w3lss">

							<?php
							$no = 0.3;
							foreach ($this->Main_model->getGaleri() as $data){
								$no = $no + 0.3;
						?>
							<div class="col-md-4 w3agile_Projects_grid wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="<?=$no;?>s">
								<div class="w3agile_Projects_image">
									<a class="sb" href="<?=base_url();?>bai-admin/uploads/<?=$data['foto_galeri'];?>" title="<?=$data['keterangan'];?>">
										<figure>
											<img src="<?=base_url();?>bai-admin/uploads/<?=$data['foto_galeri'];?>" alt="" class="img-responsive" />
											<figcaption>
												<h4><?=$data['judul_galeri'];?></h4>
												<p>
													<?=$data['keterangan'];?>
												</p>
											</figcaption>
										</figure>
									</a>
								</div>
							</div>
						<?php
						} ?>
							<div class="clearfix"> </div>
						</div>

				</div>
			</div>
		</div>
		<!--Projects-->
