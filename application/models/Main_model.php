<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_Model {

	public function tambah_data($tabel,$data){
		$this->db->insert($tabel,$data);
	}


	public function getSlideshow()
	{
		$query = $this->db->query("SELECT judul_slide, ket_slide, gambar_slide, link_slide FROM slideshow");
		return $query->result_array();
	}

	public function getMengapaGabung()
	{
		$query = $this->db->query("SELECT judul, isi_singkat FROM gabung_bai");
		return $query->result_array();
	}

	public function getSambutan()
	{
		$query = $this->db->query("SELECT foto_sambutan, isi_sambutan, nama_ketua, jabatan FROM sambutan_ketua");
		return $query->result_array();
	}

	public function getKegiatanRutin()
	{
		$query = $this->db->query("SELECT nama_kegiatan, deskripsi, hari, cp, logo_icon, jam_kegiatan FROM kegiatan_rutin");
		return $query->result_array();
	}

	public function getGaleri()
	{
		$query = $this->db->query("SELECT judul_galeri, keterangan, tahun_foto, foto_galeri FROM galeri ");
		return $query->result_array();
	}

	public function getContactMin()
	{
		$query = $this->db->query("SELECT no_hp, alamat, email FROM info_contact");
		return $query->result_array();
	}
	public function getContactFull()
	{
		$query = $this->db->query("SELECT * FROM info_contact");
		return $query->result_array();
	}
	public function getFaq()
	{
		$query = $this->db->query("SELECT * FROM faq");
		return $query->result_array();
	}

	//--> EVENT
	public function getEvent()
	{
		$query = $this->db->query("SELECT id_event, nama_event, lokasi_event, tanggal_pelaksanaan, foto_event, jenis_pe FROM event, peserta_event WHERE peserta_event.id_pe = event.id_pe ORDER BY id_event DESC");
		return $query->result_array();
	}

	public function getDetailEvent($id)
	{
		//sasasas
		return $this->db->query("	SELECT nama_event, desk_event, foto_event, jenis_pe, nama_user, tanggal_pelaksanaan, lokasi_event, tgl_publikasi, tag_tambahan
									FROM event, peserta_event, user
									WHERE event.`id_pe` = peserta_event.`id_pe` AND user.`id_user` = event.`id_user` AND event.`id_event` = '$id'")
                        ->row_array();
	}

	public function getDetailArtikel($id)
	{
		//sasasas
		return $this->db->query("SELECT id_artikel, nama_user,  judul_artikel, gambar_artikel, tanggal_post, judul_kategori, isi_artikel FROM artikel, user, kategori_artikel WHERE user.`id_user` = artikel.`id_user` AND kategori_artikel.`id_kategori` = artikel.`id_kategori` AND artikel.`id_artikel` = '$id' ORDER BY id_artikel DESC")
                        ->row_array();
	}

	//--> ARtiKEL
	public function getArtikel()
	{
		$query = $this->db->query("	SELECT id_artikel, nama_user,  judul_artikel, gambar_artikel, tanggal_post, judul_kategori
									FROM artikel, user, kategori_artikel
									WHERE user.`id_user` = artikel.`id_user` AND kategori_artikel.`id_kategori` = artikel.`id_kategori`");
		return $query->result_array();
	}

	//periksa nim
	function cek_nim($nim)
	{
    	$this->db->select('nim');
    	$this->db->from('calon_anggota');
    	$this->db->where('nim', $nim);
    	$query = $this->db->get();
    	$result = $query->result_array();
    	return $result;
	}

}

/* End of file Main.php */
/* Location: ./application/models/Main.php */
