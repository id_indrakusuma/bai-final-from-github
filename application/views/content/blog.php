<!--special-->
<div class="content">
				<div class="special-w3">
				<h3 class="tittle1 wow fadeInRight">Artikel Badan Amalan Islam</h3>
					<div class="container">
						<div class="special-grids">

						<?php
							$no = 0.3;
							foreach ($this->Main_model->getArtikel() as $data) {
							$tanggal = $data['tanggal_post'];
							$no = $no + 0.3;
						?>
							<div class="col-md-4 special-grid wow fadeIn"  data-wow-duration="1.5s" data-wow-delay="<?=$no;?>s">
								<div class="special1">
									<img src="<?=base_url(); ?>bai-admin/uploads/<?=$data['gambar_artikel'];?>" class="img-responsive" alt=""/>
									<div class="special-icon hvr-sweep-to-top">
										<a href="<?=base_url();?>main/detailArtikel/<?=$data['id_artikel'];?>"><i class="glyphicon glyphicon-th" aria-hidden="true"></i></a>
									</div>
								</div>
								<div class="special-bottom">
									<h4><?=$data['judul_artikel'];?></h4>
									<p>
										<i class="glyphicon glyphicon-calendar"></i> <?=date("l, d F Y", strtotime($tanggal));?><br>
										<i class="glyphicon glyphicon-tags"></i> <?=$data['judul_kategori'];?><br>
										<i class="glyphicon glyphicon-user"></i> <?=$data['nama_user'];?><br>
									</p>
								</div>
							</div>
						<?php }
						?>

							<div class="clearfix"></div>
						</div>
					</div>
				</div>
		</div>
			<!--special-->
