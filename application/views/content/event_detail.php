	<?php
		$id = $this->uri->segment(3);
    	$data = $this->Main_model->getDetailEvent($id);
    	$tanggal = $data['tanggal_pelaksanaan'];
	 ?>
	<!--contact-->
		<div class="content">
			<div class="contact-w3l">
				<h2 class="tittle wow fadeIn"><?=$data['nama_event'];?></h2>
				<div class="container">

					<div class="contact-grids">

						<div class="col-md-6 contact-right wow fadeInRight">
						<img src="<?=base_url();?>bai-admin/uploads/<?=$data['foto_event'];?>" class="img-responsive" alt=""/><br>
							<ul>
								<li><i class="glyphicon glyphicon-bold" aria-hidden="true"></i> <?=$data['nama_event'];?></li>
								<li><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i> Dilaksanakan pada <?=date("l, d F Y", strtotime($tanggal));?></li>
								<li><i class="glyphicon glyphicon-user" aria-hidden="true"></i> <?=$data['jenis_pe'];?></li>
								<li><i class="glyphicon glyphicon-tags" aria-hidden="true"></i> <?=$data['tag_tambahan'];?></li>

							</ul>
							<hr>
						</div>

						<div class="col-md-6 contact-left wow fadeInLeft">
						<p><?=$data['desk_event'];?></p>
							<br>
							Dipublikasikan pada <b><i><?=$data['tgl_publikasi'];?></i></b><br>
							Ditulis oleh <b><i><?=$data['nama_user'];?></i></b>

						</div>
						<div class="clearfix"></div>

					</div>

				</div>
			</div>
		</div>
		<!--contact-->
