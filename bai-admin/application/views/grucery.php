<!--
========================================================
This Website using original template from AdminLTE
And redesign by Indra Kusuma
Author : Indra Kusuma
Website : www.indrakusuma.web.id
Email : indrakusuma.udinus@gmail.com
HAPPY CODING!
=========================================================
-->
<!DOCTYPE html>
<html">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?=$title;?> | <?=$_SESSION['role_user']; ?></title>
	<meta name="description" content="System Backend BAI">
	<meta name="author" content="Indra Kusuma">

	<!-- link css for external -->
	<!-- Bootstrap 3.3.6 -->
	  <link rel="stylesheet" href="<?=base_url();?>assets/bootstrap/css/bootstrap.min.css">
	  <!-- Font Awesome -->
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	  <!-- Ionicons -->
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">


	   <!-- Theme style -->
	  <link rel="stylesheet" href="<?=base_url();?>assets/css/AdminLTE.css">
	  <!-- CSS Skin | Indra Kusuma -->
	  <link rel="stylesheet" href="<?=base_url();?>assets/css/skins/skin-green.css">
	  <?php
	  	//grucery custom
		    if(isset($gcrud)){
		        foreach($css_files as $file){ ?>
		    	   <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" /><?php
		        }
		        foreach($js_files as $file){ ?>
		    	   <script src="<?php echo $file; ?>"></script><?php
		        }
		    }

	   ?>


</head>
<body class="hold-transition skin-green sidebar-mini">
	<div class="wrapper">
		<?php
			//menu -> menyesuiakan dengan tipe
			//echo $_SESSION['role_user'];
			if ($_SESSION['role_user'] == "superadmin") {
				$this->load->view('layout/header_super');
				$this->load->view('layout/menu_super');
			}else{
				$this->load->view('layout/header_umum');
				$this->load->view('layout/menu_umum');
			}
			//content -> isi
			$this->load->view("$content");
			//copyright
			//footer
			$this->load->view('layout/footer');
		 ?>
	</div>



	<!-- jQuery 2.2.3 -->
	<!-- AdminLTE App -->
	<script src="<?=base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="<?=base_url();?>assets/plugins/fastclick/fastclick.js"></script>
	<script src="<?=base_url();?>assets/js/app.min.js"></script>

</body>
</html>
