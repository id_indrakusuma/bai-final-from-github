<script>
  $(function () {
    $("#user_log").DataTable({
    	"ordering": false
    });
    $('#kategori').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>