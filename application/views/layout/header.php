<?php
	$url = $this->uri->segment(1);
?>
<!--header | Indra Kusuma-->
	<div class="header" id="home">
			<div class="header-top">
				<div class="container">
					<div class="logo wow fadeIn" data-wow-duration="2s">
						<h1><a href="<?=base_url();?>">BAI Matholi'ul Anwar<span>Universitas Dian Nuswantoro Semarang</span></a></h1>
					</div>
				</div>
			</div>
		<div class="container">
			<div class="header-bottom">
				<nav class="navbar navbar-default">
					<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<nav class="menu menu--francisco">
								<ul class="nav navbar-nav menu__list">
									<li class="menu__item <?php if($url==""){echo 'menu__item--current';}?>"><a href="<?=base_url();?>" class="menu__link"><span class="menu__helper">Home</span></a></li>
									<li class="menu__item <?php if($url=="tentang"){echo 'menu__item--current';}?>"><a href="<?=base_url();?>tentang" class="menu__link <?php if($url=="about"){echo 'menu__item--current';}?>"><span class="menu__helper">About</span></a></li>
									<li class="menu__item <?php if($url=="event"){echo 'menu__item--current';}?>"><a href="<?=base_url();?>event" class="menu__link"><span class="menu__helper">Event</span></a></li>
									<li class="menu__item <?php if($url=="galeri"){echo 'menu__item--current';}?>"><a href="<?=base_url();?>galeri" class="menu__link"><span class="menu__helper">Gallery</span></a></li>
									<li class="menu__item <?php if($url=="blog"){echo 'menu__item--current';}?>"><a href="<?=base_url();?>blog" class="menu__link"><span class="menu__helper">Blog</span></a></li>
									<li class="menu__item <?php if($url=="contact"){echo 'menu__item--current';}?>"><a href="<?=base_url();?>contact" class="menu__link"><span class="menu__helper">Contact</span></a></li>
									<li class="menu__item <?php if($url=="daftar"){echo 'menu__item--current';}?>"><a href="<?=base_url();?>daftar" class="menu__link"><span class="menu__helper">Join Now!</span></a></li>

								</ul>
							</nav>
								<div class="social-icons">
									<a href="https://www.facebook.com/bai.matholiulanwar" title="Follow us on Facebook!"><i class="icon"></i></a>
									<a href="https://twitter.com/BAI_UDINUS" title="Follow us on Twitter!"><i class="icon1"></i></a>

								</div>
							<div class="clearfix"></div>
						</div><!-- /.navbar-collapse -->
							<!-- /.container-fluid -->
					</div>
				</nav>

			</div>
		</div>
	</div>
	<!--header-->
