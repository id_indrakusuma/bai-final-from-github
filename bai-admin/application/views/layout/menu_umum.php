
<?php
//-> URL for Kondisi
  $url = $this->uri->segment(2);
  $url2 = $this->uri->segment(1);
?>
 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <?php
          if ($_SESSION['foto_user'] == "") {
            ?>
              <img src="<?=base_url();?>assets/img/not_ready.png" class="img-circle" alt="User Image">
            <?php
          }
          else{
            ?>
              <img src="<?=base_url();?>uploads/<?=$_SESSION['foto_user'];?>" class="img-circle" alt="User Image">
            <?php
          }
          ?>
        </div>
        <div class="pull-left info">
          <p><?=$_SESSION['nama_user'] ;?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?=base_url();?>">
            <i class="fa fa-dashboard"></i> <span>Beranda</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">Home</small>
            </span>
          </a>
        </li>
        <?php
          if ($_SESSION['role_user'] == 'admin') {
            //muncul ketika role admin
            ?>
            <li <?php if($url=="artikel"){echo 'class="active"';}?>><a href="<?=base_url();?>grud/artikel"><i class="fa fa-arrow-circle-right"></i> Artikel</a></li>
            <li <?php if($url=="kegiatan_rutin"){echo 'class="active"';}?>><a href="<?=base_url();?>grud/kegiatan_rutin"><i class="fa fa-arrow-circle-right"></i> Kegiatan Rutin</a></li>
            <li <?php if($url=="event"){echo 'class="active"';}?>><a href="<?=base_url();?>grud/event"><i class="fa fa-arrow-circle-right"></i> Event</a></li>

			      <li <?php if($url=="slideshow"){echo 'class="active"';}?>><a href="<?=base_url();?>grud/slideshow"><i class="fa fa-arrow-circle-right"></i> Slide Show</a></li>
            <li <?php if($url=="sambutan"){echo 'class="active"';}?>><a href="<?=base_url();?>grud/sambutan"><i class="fa fa-arrow-circle-right"></i> Sambutan Ketua</a></li>
            <li <?php if($url=="galeri"){echo 'class="active"';}?>><a href="<?=base_url();?>grud/galeri"><i class="fa fa-arrow-circle-right"></i> Galeri Kegiatan</a></li>
            <li <?php if($url=="gabung_bai"){echo 'class="active"';}?>><a href="<?=base_url();?>grud/gabung_bai"><i class="fa fa-arrow-circle-right"></i> Mengapa Gabung BAI ?</a></li>
            <li <?php if($url=="tentang"){echo 'class="active"';}?>><a href="<?=base_url();?>grud/tentang"><i class="fa fa-arrow-circle-right"></i> Tentang BAI</a></li>
            <li <?php if($url=="faq"){echo 'class="active"';}?>><a href="<?=base_url();?>grud/faq"><i class="fa fa-arrow-circle-right"></i> FAQ</a></li>
            <li <?php if($url=="info_contact"){echo 'class="active"';}?>><a href="<?=base_url();?>grud/info_contact"><i class="fa fa-arrow-circle-right"></i> Info Contact</a></li>
            <li <?php if($url=="feedback"){echo 'class="active"';}?>><a href="<?=base_url();?>grud/feedback"><i class="fa fa-comment"></i> <span>Feedback Pengguna</span></a></li>

            <?php //-> end of Admin
          }
          else if($_SESSION['role_user'] == 'bendahara'){
            //bendahara
            ?>
            <li class="header">BENDAHARA</li>
            <li <?php if($url=="uangkas"){echo 'class="active"';}?>><a href="<?=base_url();?>bendahara/uangkas"><i class="fa fa-arrow-circle-right"></i> Uang Kas Umum</a></li>
             <li <?php if($url=="kegiatan_rutin"){echo 'class="active"';}?>><a href="<?=base_url();?>bendahara/uangkasPeriode"><i class="fa fa-arrow-circle-right"></i> Uang Kas Periode</a></li>

            <?php
          }
          else if($_SESSION['role_user'] == 'sekretaris'){
            //untuk sekretaris
            ?>
            <li class="header">SEKRETARIS</li>

            <li <?php if($url=="evaluasiEvent"){echo 'class="active"';}?>><a href="<?=base_url();?>sekretaris/evaluasiEvent"><i class="fa fa-arrow-circle-right"></i> Evaluasi Event</a></li>
            <li <?php if($url=="inventaris"){echo 'class="active"';}?>><a href="<?=base_url();?>sekretaris/inventaris"><i class="fa fa-arrow-circle-right"></i> Inventaris BAI</a></li>
            <li <?php if($url=="suratMasuk"){echo 'class="active"';}?>><a href="<?=base_url();?>sekretaris/suratMasuk"><i class="fa fa-arrow-circle-right"></i> Surat Masuk</a></li>
             <li <?php if($url=="suratKeluar"){echo 'class="active"';}?>><a href="<?=base_url();?>sekretaris/suratKeluar"><i class="fa fa-arrow-circle-right"></i> Surat Keluar</a></li>

            <?php

          }
          else if($_SESSION['role_user'] == 'kaderisasi'){
            //untuk kaderisasi
            ?>
            <li class="header">KADERISASI </li>
            <li <?php if($url=="mutobaahbai"){echo 'class="active"';}?>><a href="<?=base_url();?>kaderisasi/mutobaahbai"><i class="fa fa-arrow-circle-right"></i> Formulir Mutobaah</a></li>
            <?php
          }
            //untuk kaderisasi
          else if($_SESSION['role_user'] == 'pengurus'){
            //untuk pengurus
            ?>
            <li <?php if($url=="mutobaahbai"){echo 'class="active"';}?> > <a href="<?=base_url();?>pengurus/mutobaah"><i class="fa fa-arrow-circle-right"></i> Formulir Mutobaah</a></li>
            <?php
          }
          else {
            //nothing
          }
        ?>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
