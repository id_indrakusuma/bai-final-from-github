<?php 
     $id = $this->uri->segment(3);
     $detail = $this->Main_model->getDataUser($id);
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small>BAI Online System</small>
      </h1>
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="<?=base_url();?>admin/updateUser" method="post">
                <!-- text input -->
                <input type="hidden" name="id_user" value="<?=$detail['id_user'];?>" >
                <div class="form-group">
                  <label>Nama Lengkap</label>
                  <input type="text" name="nama_user" value="<?=$detail['nama_user'];?>" class="form-control" placeholder="Nama Lengkap.." required="">
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" name="email_user" value="<?=$detail['email_user'];?>" class="form-control" placeholder="Email.." required="">
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="password_user" class="form-control" placeholder="Password.." required="">
                </div>
                <div class="form-group">
                  <label>Jabatan</label>
                  <select class="form-control" name="role">
                    <?php if ($detail['role'] == "admin") {
                        ?>
                      <option value="admin" selected>Admin</option>
                        <?php }
                      else{
                        ?>
                        <option value="pengurus" selected>Pengurus</option>
                        <?php }
                    ?>
                  </select>
                </div>
                <div>
                  <input type="submit" class="btn btn-md btn-primary" value="Update Data">
                  <input type="reset" class="btn btn-md btn-warning" value="Reset">
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
    </section>
    <!-- /.content -->
  </div>