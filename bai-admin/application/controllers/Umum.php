<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Umum extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Do your magic here
    if(!isset($_SESSION)){
            session_start();
        }
        if (!isset($_SESSION['id_user'])) {
          redirect(base_url());
        }
  }

  function logout()
  {
    //-> system log
        $tabel = 'system_log';
        $data = array(
                'id_user' => $_SESSION['id_user'],
                'aktifitas' => 0 //-> 1 untuk login dan 0 untuk logout
            );
        $this->Main_model->tambah_data($tabel,$data);
    session_destroy();
    redirect(base_url());
  }

}
