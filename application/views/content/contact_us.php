	<!--contact-->
		<div class="content">
			<div class="contact-w3l">
				<h2 class="tittle wow fadeInRight">Hubungi Kami</h2>
				<div class="map wow fadeIn">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.240543552859!2d110.40634406477312!3d-6.980917344956921!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e708b4bff012b85%3A0x21f57d5002768ec0!2sUKM+BAI+Matholi&#39;ul+Anwar+UDINUS!5e0!3m2!1sid!2sid!4v1482303861743" style="border:0" allowfullscreen></iframe>
				</div>

				<div class="container">

				<?php foreach ($this->Main_model->getContactFull() as $data){
					?>

					<div class="contact-grids">
						<div class="contact-form wow fadeIn" data-wow-duration="2s" data-wow-delay="1.5s">

							<h4>Contact Form</h4>
							<?php
								$info = $this->session->flashdata('info');
								if (isset($info)) {
									?>
										<div class="alert alert-success wow bounceIn" role="alert" data-wow-duration="4s">
											<h4 style="font-size:16px;"><strong> <?=$info;?> </strong></h4>
										</div>
									<?php
								}
							?>
							<form action="<?=base_url();?>main/contact_add" method="post">
							<input type="text" name="nama" required="" placeholder="Nama lengkap">
							<input type="mail" name="email" required="" placeholder="Email">
							<input type="text" name="telepon" required="" placeholder="Telepon">
							<textarea name="pesan" required="" placeholder="Pesan.."></textarea>
							<input type="submit" value="Submit" >
							<input type="reset" value="Clear" >
						</form>
					</div>
						<div class="col-md-12 contact-right wow fadeIn" data-wow-duration="2s" data-wow-delay="0.5s">
						<h4>More Info</h4>
							<ul>
								<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> Office : <?=$data['no_telp'];?></li>
								<li><i class="glyphicon glyphicon-phone" aria-hidden="true"></i> Mobile : <?=$data['no_hp'];?></li>
								<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i> <a href="#"><a href="mailto:<?=$data['email'];?>"><?=$data['email'];?></a></a></li>
								<li><i class="glyphicon glyphicon-print" aria-hidden="true"></i> Fax : <?=$data['fax'];?></li>
								<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i><?=$data['alamat'];?></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<?php
				}?>


				</div>
			</div>
		</div>
		<!--contact-->
