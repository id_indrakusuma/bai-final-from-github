<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sekretaris extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!isset($_SESSION)){
            session_start();
        }
				$ur = $_SESSION['role_user'];
				//kondisi
				// jika role bukan admin dan juga bukan pengurus maka akan diredirect
				if ($ur != 'superadmin' && $ur != 'sekretaris') {
					redirect(base_url());
				}

        //load library GRUCERY CRUD
        $this->load->library('grocery_CRUD');
        $this->crud = new grocery_CRUD();

	}
	public function index()
	{
		$data['title']		= "Dashboard";
		$data['custom_css']	= null;
		$data['custom_js']	= null;
		$data['custom_script']	= null;
    $data['content']	= "content/dashboard";

		$this->load->view('Main', $data);
	}

	public function suratMasuk(){
		//surat Masuk
		$user_aktif = $_SESSION['id_user'];
		$this->crud->set_table('surat_masuk');
		$this->crud->set_subject('Daftar Surat Masuk BAI');

		$this->crud->columns('no_urut','no_surat','perihal_surat','instansi_pengirim','tgl_masuk','id_user');
		$this->crud->display_as('no_urut','Nomor Urut');

		$this->crud->display_as('perihal_surat','Perihal');
		$this->crud->display_as('no_surat','Nomor Surat');
		$this->crud->display_as('instansi_pengirim','Instansi Pengirim');
		$this->crud->display_as('tgl_masuk','Tanggal Masuk');
		$this->crud->display_as('id_user','Publiser');

		//required
		$this->crud->add_fields('no_surat','instansi_pengirim','alamat_pengirim','tgl_masuk','perihal_surat','id_user','keterangan');
		$this->crud->required_fields('no_surat','instansi_pengirim','tgl_masuk','perihal_surat','id_user');
		$this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));

		$this->crud->unset_read();

        $output = $this->crud->render();
        $output->gcrud = 1;
        $output->title = "Surat Masuk BAI";
        $output->content = "content/blank";

		$this->load->view('grucery', $output);

	}

	public function suratKeluar(){

		$user_aktif = $_SESSION['id_user'];
		$this->crud->set_table('surat_keluar');
		$this->crud->set_subject('Daftar Surat Keluar BAI');

		$this->crud->columns('no_urut','no_surat','perihal','instansi_tujuan','tanggal_keluar','id_user');
		$this->crud->display_as('no_urut','Nomor Urut');

		$this->crud->display_as('perihal','Perihal');
		$this->crud->display_as('no_surat','Nomor Surat');
		$this->crud->display_as('instansi_tujuan','Instansi Tujuan');
		$this->crud->display_as('tanggal_keluar','Tanggal Keluar');
		$this->crud->display_as('id_user','Publiser');

		//required
		$this->crud->add_fields('no_surat','instansi_tujuan','alamat_tujuan','tanggal_keluar','perihal','id_user','keterangan');
		$this->crud->required_fields('no_surat','instansi_tujuan','tanggal_keluar','perihal','id_user');
		$this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));

		$this->crud->unset_read();

        $output = $this->crud->render();
        $output->gcrud = 1;
        $output->title = "Surat Keluar BAI";
        $output->content = "content/blank";

		$this->load->view('grucery', $output);
	}

	//EVALUASI event
	public function evaluasiEvent(){
		$user_aktif = $_SESSION['id_user'];
		$this->crud->set_table('evaluasi_event');
			$this->crud->set_subject('Hasil Evaluasi Event BAI');

			//kolom
			$this->crud->columns('id_event','tanggal_eval','total_peserta_eval', 'id_user');
			$this->crud->display_as('id_event','Event');
			$this->crud->display_as('tanggal_eval','Tanggal Evaluasi');
			$this->crud->display_as('total_peserta_eval','Panitia Hadir');
			$this->crud->display_as('id_user','Penulis Evaluasi');

			//relasi crud
			$this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));
			$this->crud->set_relation('id_event','event','nama_event');

			$this->crud->add_fields('id_event','total_peserta_eval','tanggal_eval','hasil_eval','id_user');
			$this->crud->required_fields('id_event','tanggal_eval','hasil_eval','id_user');

			//API grucery untuk beberapa fungsi
			$this->crud->unset_read();

	        $output = $this->crud->render();
	        $output->gcrud = 1;
	        $output->title = "Hasil Evaluasi Event BAI";
	        $output->content = "content/blank";

			$this->load->view('grucery', $output);

	}
	public function inventaris()
	{
		$user_aktif = $_SESSION['id_user'];
		$this->crud->set_table('inventaris');
		$this->crud->set_subject('Inventaris BAI');

		$this->crud->columns('id_inventaris','id_user','nama_barang', 'tipe_barang','foto_barang');
		$this->crud->display_as('id_user', 'Publisher');
		$this->crud->display_as('id_inventaris', 'Kode Barang');
		$this->crud->required_fields('id_user','nama_barang','tipe_barang','status');
		//membatasi jumlah user yang akan dipost -> sesuai denga
		$this->crud->set_relation('id_user','user','nama_user',array('id_user' => $user_aktif));

		//edit field
		$this->crud->add_fields('id_user','nama_barang','tipe_barang','status','keterangan','foto_barang');
		//file yang boleh diedit
		$this->crud->edit_fields('id_user','nama_barang','tipe_barang','status','keterangan','foto_barang');
		//upload type path
		$this->crud->set_field_upload('foto_barang', 'uploads');

		//validasi gambar
		$this->crud->callback_before_upload(array($this, '_valid_images'));

		//kondiis untuk membatasi isi field
		$this->crud->where("inventaris.id_user",$_SESSION['id_user']);

		//API grucery untuk beberapa fungsi
		$this->crud->unset_read();

        $output = $this->crud->render();
        $output->gcrud = 1;
        $output->title = "Artikel BAI";
        $output->content = "content/blank";

		$this->load->view('grucery', $output);
	}


	//callback upload gambar || file yang diijinkan PNG|JPG|JPEG -> bisa ditambahi lagi, tinggal diberikan kondiis tambahan
		public function _valid_images($files_to_upload, $field_info)
		{
		  if ($files_to_upload[$field_info->encrypted_field_name]['type'] != ('image/png' || 'image/jpg' || 'image/jpeg'))
		  {
		   	return 'Maaf ! Gambar yang diperbolehkan hanya bertipe PNG / JPG / JPEG';
		  }
		  return true;
		}


}

/* End of file Sekretaris.php */
/* Location: ./application/controllers/Sekretaris.php */
