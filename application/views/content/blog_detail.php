	<?php
		$id = $this->uri->segment(3);
    	$data = $this->Main_model->getDetailArtikel($id);
    	$tanggal = $data['tanggal_post'];
	 ?>
	<!--contact-->
		<div class="content">
			<div class="contact-w3l">
				<h2 class="tittle wow fadeInLeft"><?=$data['judul_artikel'];?></h2><center><h4>Oleh <?=$data['nama_user'];?></h4></center>
				<div class="container wow fadeIn">

					<div class="contact-grids">
						<div class="col-md-12 contact-left">
						<center>
						 <figure><img src="<?=base_url();?>bai-admin/uploads/<?=$data['gambar_artikel'];?>" class="img-responsive" style="max-height: 400px;"/>

  						<figcaption><?=$data['judul_artikel'];?></figcaption>
						</figure> </center>
						<p><?=$data['isi_artikel'];?></p>
							<br>
							Dipublikasikan pada <b><i><?=date("l, d F Y", strtotime($tanggal));?></i></b><br>
							Ditulis oleh <b><i><?=$data['nama_user'];?></i></b>

						</div>
						<div class="clearfix"></div>

					</div>

				</div>
			</div>
		</div>
		<!--contact-->
