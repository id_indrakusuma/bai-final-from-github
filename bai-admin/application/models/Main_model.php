<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_Model {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	/*GLOBAL FUNGSI | Indra Kusuma*/
	//fungsi insert data
	public function tambah_data($tabel,$data){
		$this->db->insert($tabel,$data);
	}

	/* END GLOBAL FUNGSI | Indra Kusuma*/

	/*FUNGSI USER | Indra Kusuma*/
		//-> funsgi untuk hapus data
	function hapusUser($tabel,$id)
	{
		$this->db->query("
			UPDATE $tabel 
			SET `status_user` = '0' 
			WHERE `id_user` = $id");
	}

	function updateUser($id, $data)
	{
		$this->db->where('id_user', $id);
		$this->db->update('user', $data);
	}

	//fungsi mendapat data lengkap user | 1 record
	public function getDataUser($id)
	{
		return $this->db->query("
			SELECT * 
			FROM user 
			WHERE id_user = $id")
                        ->row_array();
	}

	//-> mendapatkan aktifitas user
	function getUserLog(){
		$query = $this->db->query("
			SELECT nama_user, aktifitas, tanggal_log 
			FROM system_log, USER 
			WHERE user.`id_user` = system_log.`id_user` 
			ORDER BY tanggal_log 
			DESC LIMIT 100");
		return $query->result_array();
	}

	function getDaftarUserBai(){
		$query = $this->db->query("
			SELECT id_user, nama_user, role, 
				(SELECT tanggal_log 
				FROM system_log 
				WHERE system_log.`id_user`=user_new.`id_user` 
				ORDER BY tanggal_log 
				DESC LIMIT 1 )  
			AS tanggal_log, status_user FROM USER AS user_new WHERE status_user = '1'");
		return $query->result_array();
	}
	/*END FUNGSI USER | Indra Kusuma*/
	

	/*MODEL UNTUK ARTIKEL | Indra Kusuma*/
	function getDaftarArtikel(){
		$query = $this->db->query("
			SELECT id_artikel, judul_artikel, nama_user, tanggal_post, judul_kategori 
			FROM artikel,USER, kategori_artikel 
			WHERE artikel.`id_user`= user.`id_user` AND kategori_artikel.`id_kategori` = artikel.`id_kategori` 
			ORDER BY tanggal_post DESC");
		return $query->result_array();
	}

	function getKategoriArtikel(){
		$query = $this->db->query("	SELECT * 
									FROM kategori_artikel 
									WHERE status_kategori = '1'");
		return $query->result_array();
	}

	function hapusArtikel($tabel,$id)
	{
		$this->db->query("
			DELETE FROM artikel 
			WHERE id_artikel = $id");
	}

	function getDataArtikel($id)
	{
		return $this->db->query("
			SELECT * FROM artikel 
			WHERE id_artikel = $id")
                        ->row_array();
	}
	
	function updateArtikel($id, $data)
	{
		$this->db->where('id_artikel', $id);
		$this->db->update('artikel', $data);
	}

	//-> kategori artikel
	function hapusKategori($id)
	{
		$this->db->query("	UPDATE kategori_artikel 
							SET status_kategori = 0 
							WHERE id_kategori = $id");
	}

	/*END MODEL ARTIKEL*/

	/* Model Event */
	function getDaftarEvent()
	{
		$query = $this->db->query("	SELECT * FROM detail_acara");
		return $query->result_array();
	}

	function getEventOn()
	{
		$query = $this->db->query("SELECT * FROM detail_acara WHERE tanggal_kegiatan >= CURDATE()");
		return $query->result_array();
	}

	function getEventOff()
	{
		$query = $this->db->query("SELECT * FROM detail_acara WHERE tanggal_kegiatan < CURDATE()");
		return $query->result_array();
	}

	//->menggunakan fitur VIEW dari database
	function getDetailEvent($id)
	{
		return $this->db->query("SELECT * FROM detail_acara WHERE id_kegiatan = $id")
		->row_array();
	}

	function getJenisEvent()
	{
		$query = $this->db->query("SELECT * from jenis_kegiatan");
		return $query->result_array();
	}

	function getPesertaEvent()
	{
		$query = $this->db->query("SELECT * FROM peserta_event");
		return $query->result_array();
	}

	function hapusEvent($id)
	{
		$this->db->query("DELETE FROM kegiatan WHERE id_kegiatan = $id");
	}

	function updateEvent($id, $data)
	{
		$this->db->where('id_kegiatan', $id);
		$this->db->update('kegiatan', $data);
	}
	/* End Model Event */
}

/* End of file Main_model.php */
/* Location: ./application/models/Main_model.php */