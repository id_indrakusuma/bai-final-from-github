<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small>BAI Online System</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <div class="row">
         <div class="col-md-12">
          <div class="box box-success">
          <div class="box-header">
              <?php
                $info = $this->session->flashdata('info');
                if (isset($info)) {
                  ?>
                    <div class="callout callout-warning">
                      <h4><?=$info;?></h4>
                    </div>
                  <?php
                }
              ?>
              <h3 class="box-title">Kegiatan Rutin</h3>
              <a href="<?=base_url();?>admin/addEvent" title="" class="btn btn-sm btn-primary btn-flat pull-right"><i class="fa fa-plus-circle"> </i> Tambah Kegiatan</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="event" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th>Nama Kegiatan</th>
                  <th>Jenis Kegiatan</th>
                  <th>Peserta</th>
                  <th>Jam Mulai</th>
                  <th>Tanggal Pelaksanaan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                
                <tbody>
                  <?php foreach ($this->Main_model->getEventOn() as $data) {
                    //-> Kondisi tambahan untuk merubah angka menjadi text yang kita inginkan
                    //-> konversi tanggal info : http://www.dumetschool.com/blog/Mengubah-Format-Tanggal-Dari-Database
                    $tanggal = $data['tanggal_kegiatan'];
                    ?>
                      <tr>
                        <td><?=$data['nama_kegiatan'];?></td>
                        <td><?=$data['jenis_kegiatan'];?></td>
                        <td><?=$data['jenis_pe'];?></td>
                        <td><?=$data['jam_mulai'];?></td>
                        <td><?=date("l, d F Y", strtotime($tanggal));?></td>
                        <td>
                        <a href="<?=base_url(); ?>Admin/ubahEvent/<?=$data['id_kegiatan'];?>" class="btn btn-xs btn-warning" title="Ubah"><i class="fa fa-pencil"></i></a>
                          <a href="<?=base_url(); ?>Admin/hapusEvent/<?=$data['id_kegiatan'];?>" class="btn btn-xs btn-danger" title="Hapus"><i class="fa fa-eraser"></i></a> 
                          <a href="<?=base_url(); ?>Admin/detailEvent/<?=$data['id_kegiatan'];?>" class="btn btn-xs btn-success" title="Detail Acara"><i class="fa fa-search"></i> Detail</a> 
                        </td>
                      
                      </tr>
                    <?php
                } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>