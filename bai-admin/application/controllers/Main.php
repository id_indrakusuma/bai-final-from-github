<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
        if(!isset($_SESSION)){
            session_start();
        }
        //-> cek login
        if(isset($_POST["login"])){
            $email = $this->input->post("email");
            $password = md5($this->input->post("password"));
            $query = $this->db->query("SELECT * FROM `user` WHERE email_user = '$email' AND password_user='$password' AND status_user = '1'");
            if($query->num_rows() > 0){
                $tmp = $query->result_array();
								$_SESSION['id_user'] = $tmp[0]['id_user'];
                $_SESSION['foto_user'] = $tmp[0]['foto_user'];
                $_SESSION['nama_user'] = $tmp[0]['nama_user'];
                $_SESSION['role_user'] = $tmp[0]['role'];
            }


        }

        //-> membuat perbedaan tingkat login
        if(isset($_SESSION['id_user'])){

            //-> system log
            $tabel = 'system_log';
            $user = $_SESSION['id_user'];
            $data = array(
                    'id_user' => $user,
                    'aktifitas' => 1 //-> 1 untuk login dan 0 untuk logout
                );
            $this->Main_model->tambah_data($tabel,$data);
						//jika berhasil -> cek role user
						if($_SESSION['role_user'] == "superadmin"){
								//superadmin menggantikan Admin
							redirect('Admin','refresh');
						}
						else if($_SESSION['role_user'] == "admin") {
							redirect('Admin','refresh');
						}
						else if($_SESSION['role_user'] == "sekretaris"){
							redirect('Sekretaris','refresh');
						}
						else if($_SESSION['role_user'] == "bendahara"){
							redirect('Bendahara', 'refresh');
						}
						else if($_SESSION['role_user'] == "kaderisasi"){
							redirect('Kaderisasi', 'refresh');
						}
						else{
							redirect('Pengurus');
						}
        }

        else{
            $this->load->view('login');
						//-> Informasi password & username tidak cocok
            $this->session->set_flashdata('info', 'Email & Password tidak cocok!');
        }

	}

    public function eror404()
    {
        $data['title']      = "Halaman Tidak Ditemukan";
        $data['custom_css'] = null;
        $data['custom_js']  = null;
        $data['custom_script']  = null;
        $data['content']    = "content/404";

        $this->load->view('404', $data);
    }

}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */
