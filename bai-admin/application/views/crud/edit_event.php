<?php 
     $id = $this->uri->segment(3);
     $detail = $this->Main_model->getDetailEvent($id);
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small>BAI Online System</small>
      </h1>
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-8">
          <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" action="<?=base_url();?>admin/updateEvent" method="post">
                <!-- text input -->
                <div class="form-group">
                  <input type="hidden" name="id_kegiatan" class="form-control" value="<?=$detail['id_kegiatan'];?>">
                  <input type="hidden" name="id_user" class="form-control" value="<?=$_SESSION['id_user'];?>">
                  <label>Nama Kegiatan</label>
                  <input type="text" name="nama_kegiatan" class="form-control" value="<?=$detail['nama_kegiatan'];?>" placeholder="Nama Kegiatan.." required="">
                </div>
                <div class="form-group">
                  <label>Jenis Kegiatan</label>
                  <select class="form-control select2" style="width: 100%;" name="jenis_kegiatan">
                    <?php foreach ($this->Main_model->getJenisEvent() as $kat) {
                      ?>
                        <option value="<?=$kat['id_jk'] ;?>"> <?=$kat['jenis_kegiatan'];?></option>
                      <?php
                    } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Peserta Kegiatan</label>
                  <select class="form-control select2" style="width: 100%;" name="peserta">
                    <?php foreach ($this->Main_model->getPesertaEvent() as $kat) {
                      ?>
                        <option value="<?=$kat['id_pe'] ;?>"> <?=$kat['jenis_pe'];?></option>
                      <?php
                    } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Tempat Pelaksanaan</label>
                  <input type="text" name="tempat" class="form-control" value="<?=$detail['tempat'];?>" placeholder="Tempat Pelaksanaan.." required="">
                </div>
                  <!-- Date -->
              <div class="form-group">
                <label>Tanggal Kegiatan</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="tanggal_kegiatan" class="form-control pull-right" value="<?=$detail['tanggal_kegiatan'];?>" id="datepicker" required="">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <div class="bootstrap-timepicker">
                <div class="form-group">
                  <label>Waktu Pelaksanaan</label>
                  <div class="input-group">
                  <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <input type="text" name="jam_mulai" class="form-control timepicker" value="<?=$detail['jam_mulai'];?>" required="">
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>
              <div class="form-group">
                  <label>Ketua / Penanggung Jawab Kegiatan</label>
                  <input type="text" name="pj_kegiatan" class="form-control" value="<?=$detail['pj_kegiatan'];?>" placeholder="Nama Lengkap.." required="">
                </div>

             <div class="form-group">
                    <?=form_textarea('deskripsi_kegiatan', $detail['deskripsi_kegiatan'], "class = 'textarea' style='width: 100%; height: 230px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;'")?>
                  </div>

                <div>
                  <input type="submit" class="btn btn-md btn-flat btn-primary pull-right" value="Update Event">
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
    </section>
    <!-- /.content -->
  </div>