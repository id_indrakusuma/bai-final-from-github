<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small>BAI Online System</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="user_log" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th>Nama User</th>
                  <th>Aktifitas</th>
                  <th>Tanggal</th>
                </tr>
                </thead>
                
                <tbody>
                  <?php foreach ($this->Main_model->getUserLog() as $data) {
                    //-> Kondisi tambahan untuk merubah angka menjadi text yang kita inginkan
                    ?>
                      <tr>
                        <td><?=$data['nama_user'];?></td>
                        <?php 
                          if ($data['aktifitas'] == 1) {
                            ?> <td>User <a href="" class="btn btn-xs btn-success">login</a></td> <?php
                          } else{
                            ?>
                              <td>User <a href="" class="btn btn-xs btn-danger">logout</a></td>
                            <?php
                          }
                        ?>
                        <td><?=$data['tanggal_log'];?></td>
                      </tr>
                    <?php
                } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>