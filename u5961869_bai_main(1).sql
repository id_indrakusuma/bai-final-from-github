-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 08 Jan 2017 pada 06.49
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u5961869_bai_main`
--
CREATE DATABASE IF NOT EXISTS `u5961869_bai_main` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `u5961869_bai_main`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `artikel`
--

DROP TABLE IF EXISTS `artikel`;
CREATE TABLE `artikel` (
  `id_artikel` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `judul_artikel` varchar(200) DEFAULT NULL,
  `isi_artikel` text,
  `gambar_artikel` varchar(200) DEFAULT NULL,
  `tanggal_post` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_kategori` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `id_user`, `judul_artikel`, `isi_artikel`, `gambar_artikel`, `tanggal_post`, `id_kategori`) VALUES
(1, 1, 'Menganalisa Perkembangan Jaman', '<div>\r\n	Everett M. Rogers dalam bukunya Communication Technology; The New Media in Society (dalam Mulyana, 1999), mengatakan bahwa dalam hubungan komunikasi di masyarakat, dikenal empat era komunikasi yaitu era tulis, era media cetak, era media telekomunikasi dan era media komunikasi interaktif. Dalam era terakhir media komunikasi interaktif dikenal media komputer, videotext dan teletext, teleconferencing, TV kabel dan sebagainya.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Marshall McLuhan dalam bukunya Understanding Media &ndash; The Extensions of Man (1999), mengemukakan ide bahwa &ldquo; medium is message&rdquo; (pesan media ya media itu sendiri). McLuhan menganggap media sebagai perluasan manusia dan bahwa media yang berbeda-beda mewakili pesan yang berbeda-beda. Media juga menciptakan dan mempengaruhi cakupan serta bentuk dari hubungan-hubungan dan kegiatan-kegiatan manusia. Pengaruh media telah berkembang dari individu kepada masyarakat. Dengan media setiap bagian dunia dapat dihubungkan menjadi desa global.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Pengaruh media yang demikian besar kepada masyarakat menghantarkan pemikiran McLuhan untuk menyampaikan Teori Determinime Teknologi yang mulanya menuai banyak kritik dan menebar berbagai tuduhan. Ada yang menuduh bahwa McLuhan telah melebih-lebihkan pengaruh media. Tetapi dengan kemajuan teknologi komunikasi massa, media memang telah sangat maju. Saat ini, media ikut campur tangan dalam kehidupan kita secara lebih cepat daripada yang sudah-sudah dan juga memperpendek jarak di antara bangsa-bangsa. Ungkapan Mcluhan tidak dapat lagi dipandang sebagaisebuah ramalan belaka. Sebagai sebuah perbandingan perkembangan teknologi media dewasa ini; dibutuhkan hampir 100 tahun untuk berevolusi dari telegraf ke teleks, tetapi hanya dibutuhkan 10 tahun sebelum faks menjadi populer. Enam atau tujuh tahun yang lalu, internet masih merupakan barang baru tetapi sekarang mereka-mereka yang tidak tahu menggunakan internet akan di anggap ketinggalan!</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Di masyarakat dapat disaksikan bahwa teknologi komunikasi terutama televisi, komputer dan internet telah mengambil alih beberapa fungsi sosial manusia (masyarakat), setiap saat kita semua menyaksikan realitas baru di masyarakat, dimana realitas itu tidak sekedar sebuah ruang yang merefleksikan kehidupan masyarakat nyata dan peta analog atau simulasi-simulasi dari suatu masyarakat tertentu yang hidup dalam media dan alam pikiran manusia, akan tetapi sebuah ruang dimana manusia bisa hidup di dalamnya. Media massa merupakan salah satu kekuatan yang sangat mempengaruhi umat manusia di abad 21. Media ada di sekeliling kita, media mendominasi kehidupan kita dan bahkan mempengaruhi emosi serta pertimbangan kita.</div>\r\n<p>\r\n	Keberadaan media dimana-mana dan juga periklanan telah mengubah pengalaman sosial dalam kehidupan masyarakat sehari-hari. Media merupakan unsur penting dalam pergaulan sosial masa kini. Kebudayaan masyarakat tidak terlepas dari media, dan budaya itu sendiri direpresentasikan dalam media.</p>\r\n', '47b57-4.png', '2016-12-13 14:55:04', 5),
(2, 1, 'Mencari Sebuah Jawaban', '<p>\r\n	Sampai saat ini, saya masih dibingungkan dengan tata keola....</p>\r\n', '6d3fc-business-intelligence-buzzwords-panda.jpg', '2016-12-13 12:58:16', 2),
(5, 1, 'Rukiyah Masal di UDINUS menuai pujian', '<p>\r\n	Hai hai hai</p>\r\n', '28287-maxresdefault.jpg', '2016-12-13 12:59:37', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `calon_anggota`
--

DROP TABLE IF EXISTS `calon_anggota`;
CREATE TABLE `calon_anggota` (
  `id_calon` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `nim` varchar(20) NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `line_bbm` varchar(30) DEFAULT NULL,
  `mentoring` enum('Sudah','Belum') DEFAULT 'Belum',
  `tanggal_daftar` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Pending','Tolak','Terima') NOT NULL DEFAULT 'Pending',
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `calon_anggota`
--

INSERT INTO `calon_anggota` (`id_calon`, `nama_lengkap`, `nim`, `jenis_kelamin`, `no_hp`, `line_bbm`, `mentoring`, `tanggal_daftar`, `status`, `id_user`) VALUES
(1, 'Indra Kusuma', 'A11.2014.08316', 'L', '08952248642', '@indra.update', 'Belum', '2016-12-26 18:43:55', 'Pending', 1),
(2, 'Zakiy Anwar F', 'A11.2014.08326', 'L', '08122334444', '@zakiyanwar', 'Belum', '2016-12-27 03:14:03', 'Pending', 1),
(3, 'M Nurul Irfan', 'A11.2014.08363', 'L', '089522248642', '@mnirfan', 'Sudah', '2017-01-01 15:18:33', 'Pending', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
CREATE TABLE `contact_us` (
  `id_pesan` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `pesan` text,
  `status_p` enum('0','1') DEFAULT '0',
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `contact_us`
--

INSERT INTO `contact_us` (`id_pesan`, `nama`, `email`, `telepon`, `pesan`, `status_p`, `id_user`) VALUES
(5, 'Indra', 'Indra ', 'iNdra', 'askaksjaksjasMessage...', '0', NULL),
(6, '12', '121', '12121', '12121212Message...', '0', NULL),
(7, 'Indra Kusuma', 'indra@gmail.com', '08912213444', 'ha hai hai BAI', '0', NULL),
(8, 'Indra Kusuma', 'indrakusuma.udinus@gmail.com', '08912213444', 'Hai, salam kenal.\r\nWeb BAI nya kece banget. Gimana cara saya agar bisa membuat website serupa ?\r\nthanks admin', '0', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `evaluasi_event`
--

DROP TABLE IF EXISTS `evaluasi_event`;
CREATE TABLE `evaluasi_event` (
  `id_eval` int(4) NOT NULL,
  `id_event` int(4) NOT NULL,
  `tanggal_eval` date NOT NULL,
  `hasil_eval` text NOT NULL,
  `id_user` int(4) NOT NULL,
  `total_peserta_eval` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `evaluasi_event`
--

INSERT INTO `evaluasi_event` (`id_eval`, `id_event`, `tanggal_eval`, `hasil_eval`, `id_user`, `total_peserta_eval`) VALUES
(1, 1, '2016-12-26', '<p>\r\n	Hasil evaluasi adalah :</p>\r\n<p>\r\n	Dekdok : Lebih banyak lagi fotonya,</p>\r\n<p>\r\n	Konsumsi : Lebih murah &amp; enak lagi makananya.</p>\r\n', 1, 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `id_event` int(4) NOT NULL,
  `id_user` int(4) DEFAULT NULL,
  `nama_event` varchar(300) DEFAULT NULL,
  `desk_event` text,
  `foto_event` varchar(100) DEFAULT NULL,
  `id_pe` int(11) DEFAULT NULL,
  `lokasi_event` varchar(100) DEFAULT NULL,
  `tanggal_pelaksanaan` date DEFAULT NULL,
  `tgl_publikasi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tag_tambahan` varchar(100) DEFAULT NULL COMMENT '# keterangan tambahan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `event`
--

INSERT INTO `event` (`id_event`, `id_user`, `nama_event`, `desk_event`, `foto_event`, `id_pe`, `lokasi_event`, `tanggal_pelaksanaan`, `tgl_publikasi`, `tag_tambahan`) VALUES
(1, 1, 'Islamic Fair UDINUS 2017', '<p>\r\n	<strong>&nbsp;SYARAT PESERTA LOMBA</strong><br />\r\n	<br />\r\n	Surat rekomendasi dari sekolah<br />\r\n	FC Kartu Pelajar<br />\r\n	Foto Ukuran 3x4 2 lembar<br />\r\n	Pendaftaran dibuka dari tanggal 12 Januari s/d 20 Februari 2015<br />\r\n	Mebayar Biaya Registrasi yang dikirimkan di rekening (1021401583 BRI Syariah a/n Septiani Zahrun Safitri) atau bisa dibayar saat Technikal meeting.</p>\r\n<ol>\r\n	<li>\r\n		<strong>REBANA : 100.000</strong></li>\r\n	<li>\r\n		<strong>DAI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 50.000</strong></li>\r\n	<li>\r\n		<strong>TILAWAH : 50.000</strong></li>\r\n	<li>\r\n		<strong>NASYID&nbsp;&nbsp; : 100.000</strong></li>\r\n</ol>\r\n<p>\r\n	Mengisi Formulir pendaftaran.<br />\r\n	Registrasi Ulang dilaksanakan pada saat technical meeting&nbsp; pada tanggal 27 Februari 2015.<br />\r\n	Semua berkas dibawa saat TM.<br />\r\n	<br />\r\n	Cara Pendaftaran<br />\r\n	Ketik : Reg<spasi>Nama lomba<spasi>nama sekolah. Kirim ke 083838323280<br />\r\n	&nbsp;</spasi></spasi></p>\r\n', '34d99-spring_rain_wallpaper.jpg', 4, 'AULA GEDUNG E Lantai 3, Universitas Dian Nuswantoro', '2016-12-09', '2016-12-08 12:10:56', '#BAI_UDINUS #UDINUS_HITS'),
(2, 1, 'BAI Rukiyah Masal', '<p>\r\n	ini merupakan deskripsi event</p>\r\n', 'd65b3-maxresdefault.jpg', 3, 'Masjid Gedung H, Universitas Dian Nuswantoro', '2017-12-23', '2016-12-07 17:00:00', '#BAI_RUKIYAH'),
(3, 1, 'UDINUS Bersholawat', '<p>\r\n	ini acara baru</p>\r\n', 'ae71e-photo330168275532097604.jpg', 1, 'Semarang', '2016-12-23', '2016-12-19 17:00:00', '#BAI_UDINUS #SHOLAWAT');

-- --------------------------------------------------------

--
-- Struktur dari tabel `faq`
--

DROP TABLE IF EXISTS `faq`;
CREATE TABLE `faq` (
  `id_faq` int(11) NOT NULL,
  `judul_pertanyaan` varchar(400) DEFAULT NULL,
  `penjelasan` text,
  `id_user` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `faq`
--

INSERT INTO `faq` (`id_faq`, `judul_pertanyaan`, `penjelasan`, `id_user`) VALUES
(1, 'Apa sih UKM BAI itu ?', '<p>\r\n	UKM BAI merupakan sebuah Unit Kegiatan Mahasiswa Universitas Dian Nuswantoro yang bergerak pada bidang Rohani atau Keagamaan. BAI merupakan singkatan dari Badan Amalan Islam</p>\r\n', 1),
(2, 'Kenapa Saya Harus Gabung BAI ?', '<p>\r\n	Banyak hal yang bisa Anda dapatkan, salah satunya adalah meningkatkan ketaqwaan kita kepada Allah SWT.</p>\r\n', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gabung_bai`
--

DROP TABLE IF EXISTS `gabung_bai`;
CREATE TABLE `gabung_bai` (
  `id_gabung` int(3) NOT NULL,
  `judul` varchar(200) DEFAULT NULL,
  `isi_singkat` varchar(200) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gabung_bai`
--

INSERT INTO `gabung_bai` (`id_gabung`, `judul`, `isi_singkat`, `id_user`) VALUES
(1, 'Menjadi generasi islam pembaharu bangsa.', 'Dengan begabung BAI maka akan mendapatkan ilmu-ilmu islami lebih lanjut.', 1),
(2, 'Mengasah dan meningkatkan softskill.', 'Dengan bergaung ke BAI kita akan diajarkan untuk mengasah dan meningkatkan softskill kita.', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `galeri`
--

DROP TABLE IF EXISTS `galeri`;
CREATE TABLE `galeri` (
  `id_galeri` int(3) NOT NULL,
  `id_user` int(4) DEFAULT NULL,
  `judul_galeri` varchar(100) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `tahun_foto` int(4) DEFAULT NULL,
  `foto_galeri` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `galeri`
--

INSERT INTO `galeri` (`id_galeri`, `id_user`, `judul_galeri`, `keterangan`, `tahun_foto`, `foto_galeri`) VALUES
(2, 1, 'Islamic Fair', 'Islamic Fair merupakan kegiatan rutin yang dilaksanakan oleh BAI. Foto merupakan Qasidah BAI 2015', 2014, '686d7-bai-udinus.jpg'),
(3, 1, 'Panitia Islamic Fair 2016', 'Foto Bareng', 2016, 'd4683-bai-mengapa-gabung.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `info_contact`
--

DROP TABLE IF EXISTS `info_contact`;
CREATE TABLE `info_contact` (
  `id_info_contact` tinyint(1) NOT NULL,
  `ket_tambahan` text,
  `no_hp` varchar(20) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `info_contact`
--

INSERT INTO `info_contact` (`id_info_contact`, `ket_tambahan`, `no_hp`, `no_telp`, `email`, `fax`, `alamat`) VALUES
(1, '<p>\r\n	ini adalah info tambahan yang disediakan oleh user&nbsp;ini adalah info tambahan yang disediakan oleh user&nbsp;ini adalah info tambahan yang disediakan oleh user&nbsp;ini adalah info tambahan yang disediakan oleh user&nbsp;ini adalah info tambahan yang disediakan oleh user&nbsp;ini adalah info tambahan yang disediakan oleh user&nbsp;ini adalah info tambahan yang disediakan oleh user&nbsp;ini adalah info tambahan yang disediakan oleh user&nbsp;ini adalah info tambahan yang disediakan oleh user .</p>\r\n', '0895-2222-1212', '(024) - 111234', 'bai@dinus.ac.id', '(024) - 121234', 'Gedung F Lantai 2 Universitas Dian Nuswantoro, Jl. Nakula No. 5-11 Semarang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris`
--

DROP TABLE IF EXISTS `inventaris`;
CREATE TABLE `inventaris` (
  `id_inventaris` int(4) NOT NULL,
  `id_user` int(4) DEFAULT NULL,
  `nama_barang` varchar(100) DEFAULT NULL,
  `tipe_barang` enum('Habis Pakai','Tahan Lama') DEFAULT NULL,
  `status` enum('Tersedia','Tidak Tersedia') DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `tanggal_penambahan` timestamp NULL DEFAULT NULL,
  `foto_barang` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `id_user`, `nama_barang`, `tipe_barang`, `status`, `keterangan`, `tanggal_penambahan`, `foto_barang`) VALUES
(1, 1, 'Galon AQUA', 'Tahan Lama', 'Tersedia', 'Ditaruh dicamp, untuk minum komunitas', NULL, 'ce3f7-galon.jpg'),
(2, 1, 'Block Note Seminar IF2017', 'Tahan Lama', 'Tersedia', 'Diletakan dimeja, ada 10 buah', NULL, '501e2-flat_design___workspace_by_patrickroelofs-d7pfknf.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_artikel`
--

DROP TABLE IF EXISTS `kategori_artikel`;
CREATE TABLE `kategori_artikel` (
  `id_kategori` int(11) NOT NULL,
  `judul_kategori` varchar(100) DEFAULT NULL,
  `status_kategori` enum('0','1') DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori_artikel`
--

INSERT INTO `kategori_artikel` (`id_kategori`, `judul_kategori`, `status_kategori`) VALUES
(1, 'Agama', ''),
(2, 'Organisasi', '1'),
(4, 'Religi', '0'),
(5, 'Berita BAI', '1'),
(6, 'Kabar UDINUS', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatan_rutin`
--

DROP TABLE IF EXISTS `kegiatan_rutin`;
CREATE TABLE `kegiatan_rutin` (
  `id_kegitan_rutin` tinyint(2) NOT NULL,
  `nama_kegiatan` varchar(100) DEFAULT NULL,
  `deskripsi` varchar(200) DEFAULT NULL,
  `hari` enum('Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu') DEFAULT NULL,
  `cp` varchar(100) DEFAULT NULL,
  `logo_icon` varchar(100) DEFAULT NULL COMMENT 'ambil bootstrap',
  `jam_kegiatan` varchar(15) DEFAULT NULL,
  `id_user` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kegiatan_rutin`
--

INSERT INTO `kegiatan_rutin` (`id_kegitan_rutin`, `nama_kegiatan`, `deskripsi`, `hari`, `cp`, `logo_icon`, `jam_kegiatan`, `id_user`) VALUES
(1, 'Gerakan Shubuh Berjamaah', 'merupakan gerakan shubuh berjamaah yang dilaksanaakan deangn cara berjamaah', 'Jumat', '08912221333', 'glyphicon glyphicon-globe', '04.00 WIB', 1),
(2, 'Sharing Kamis Sore', 'Kejian setiap kamis sore yang setiap minggunya akan diisi oleh ustad-ustad yang berganti-ganti dan memiliki  tema yang berganti setiap minggunya.', 'Kamis', '085870388779', 'glyphicon glyphicon-share-alt', '16.00 WIB', 1),
(3, 'Tadarus Qur''an Di Kelas', 'Tadarus Qur''an Di Kelas ini berisi mengenai pelajaran Al-Qur''an untuk menghafal dan memperbaiki Tajwid anggota', 'Selasa', 'Zakiy Anwar F', 'glyphicon glyphicon-credit-card', '09.00 WIB', 1),
(4, 'Kantin Anisa', 'Kajian rutin anisa adalah sebuah kegiatan kajian khusus untuk para kaum perempuan diamana pembahasannya adalah untuk meningkatkan kualitas-kualitas perempuan', 'Jumat', 'Amalia', 'glyphicon glyphicon-apple', '11.30 WIB', 1),
(5, 'Sekolah Peradaban', 'Sebuah forum untuk membina soft skill, pengetahuan tentang islam dan leadership. Dimana setiap mahasiswa Udinus bisa mengikuti. Kegiatan ini diadakan setiap 2 minggu sekali. ', 'Sabtu', 'Rumaisha', 'glyphicon glyphicon-link', '07.30 WIB', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mutobaah`
--

DROP TABLE IF EXISTS `mutobaah`;
CREATE TABLE `mutobaah` (
  `id_mutobaah` int(6) NOT NULL,
  `id_user` int(4) NOT NULL,
  `id_bid_mutobaah` int(2) NOT NULL,
  `s_limaWaktu` enum('Ya','Tidak') NOT NULL,
  `s_sunahRawatib` enum('Ya','Tidak') NOT NULL,
  `s_qiyamulail` enum('Ya','Tidak') NOT NULL,
  `s_sunahDhuha` enum('Ya','Tidak') NOT NULL,
  `puasaSunah` enum('Ya','Tidak') NOT NULL,
  `sedekah` enum('Ya','Tidak') NOT NULL,
  `baca_quran` int(4) NOT NULL,
  `tgl_mutobaah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mutobaah`
--

INSERT INTO `mutobaah` (`id_mutobaah`, `id_user`, `id_bid_mutobaah`, `s_limaWaktu`, `s_sunahRawatib`, `s_qiyamulail`, `s_sunahDhuha`, `puasaSunah`, `sedekah`, `baca_quran`, `tgl_mutobaah`) VALUES
(1, 1, 2, 'Ya', 'Ya', 'Ya', 'Tidak', 'Ya', 'Ya', 12, '2016-12-26 16:47:43'),
(2, 4, 1, 'Ya', 'Ya', 'Ya', 'Ya', 'Ya', 'Ya', 2, '2016-12-26 17:02:07'),
(3, 4, 1, 'Ya', 'Ya', 'Tidak', 'Ya', 'Tidak', 'Ya', 2, '2016-12-27 03:12:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mutobaah_bidang`
--

DROP TABLE IF EXISTS `mutobaah_bidang`;
CREATE TABLE `mutobaah_bidang` (
  `id_bid_mutobaah` int(11) NOT NULL,
  `nama_bidang` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mutobaah_bidang`
--

INSERT INTO `mutobaah_bidang` (`id_bid_mutobaah`, `nama_bidang`) VALUES
(1, 'Bidang Pekaderan\r\n'),
(2, 'Bidang Publikasi'),
(3, 'Bidang Umum');

-- --------------------------------------------------------

--
-- Struktur dari tabel `periode`
--

DROP TABLE IF EXISTS `periode`;
CREATE TABLE `periode` (
  `id_periode` int(11) NOT NULL,
  `tahun_periode` varchar(20) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `periode`
--

INSERT INTO `periode` (`id_periode`, `tahun_periode`, `tanggal`) VALUES
(1, '2016 - 2017', '2016-12-22 22:18:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peserta_event`
--

DROP TABLE IF EXISTS `peserta_event`;
CREATE TABLE `peserta_event` (
  `id_pe` int(11) NOT NULL,
  `jenis_pe` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peserta_event`
--

INSERT INTO `peserta_event` (`id_pe`, `jenis_pe`) VALUES
(1, 'Pengurus BAI'),
(2, 'Anggota BAI'),
(3, 'UMUM'),
(4, 'SMA/SMK/Se-Derajat'),
(5, 'SMP/SLTP'),
(6, 'TK/SD');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sambutan_ketua`
--

DROP TABLE IF EXISTS `sambutan_ketua`;
CREATE TABLE `sambutan_ketua` (
  `id_sambutan` int(3) NOT NULL,
  `foto_sambutan` varchar(100) DEFAULT NULL,
  `isi_sambutan` text,
  `nama_ketua` varchar(100) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sambutan_ketua`
--

INSERT INTO `sambutan_ketua` (`id_sambutan`, `foto_sambutan`, `isi_sambutan`, `nama_ketua`, `jabatan`, `id_user`) VALUES
(1, '52c2d-a11.2015.09136.jpg', '<p>\r\n	Sebagai salah satu bagian dari BAI Matholi&#39;ul Anwar Universitas Dian Nuswantoro saya merasa bangga bisa memimpin dan bersama-sama membangun suasana islami dalam civitas akademika Universitas Dian Nuswantoro. Membantu menyediakan kegiatan-kegiatan penyejuk jiwa/rohani agar dalam kehidupan ataupun perkuliahan bisa terus semangat.</p>\r\n', 'Raras Bayu Asrori', 'Teknik Informatika', 1),
(2, '3ae60-d11.2014.01896.jpg', '<p>\r\n	Sebagai bagian dari anisa BAI Matholi&#39;ul Anwar 2016/2017 saya merasa bangga bisa memimpin dan bersama-sama meningkatkan suasana islami bagi para mahasiswi universitas Dian Nuswantoro. Harapan saya semoga kegiatan-kegiatan bagi mahasiswi-mahasiswi dapat memperbaiki kualitas mahasiswi terhadap kewajiban-kewajibannya sebagai perempuan.</p>\r\n', 'Wenny Malisa', 'Kesehatan Masyarakat', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `slideshow`
--

DROP TABLE IF EXISTS `slideshow`;
CREATE TABLE `slideshow` (
  `id_slide` int(3) NOT NULL,
  `judul_slide` varchar(100) DEFAULT NULL,
  `ket_slide` text,
  `gambar_slide` varchar(100) DEFAULT NULL,
  `link_slide` varchar(200) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `pembuatan` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `slideshow`
--

INSERT INTO `slideshow` (`id_slide`, `judul_slide`, `ket_slide`, `gambar_slide`, `link_slide`, `id_user`, `pembuatan`) VALUES
(2, NULL, NULL, '55a62-photo882817475743426478.jpg', NULL, 1, '2016-12-20 04:04:30'),
(3, NULL, NULL, 'cb7e4-photo882817475743426477.jpg', NULL, 1, '2016-12-20 06:18:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat_keluar`
--

DROP TABLE IF EXISTS `surat_keluar`;
CREATE TABLE `surat_keluar` (
  `no_urut` int(11) NOT NULL,
  `no_surat` varchar(20) NOT NULL,
  `alamat_tujuan` varchar(200) NOT NULL,
  `tanggal_keluar` date NOT NULL,
  `perihal` varchar(300) NOT NULL,
  `keterangan` varchar(300) NOT NULL,
  `instansi_tujuan` varchar(100) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `surat_keluar`
--

INSERT INTO `surat_keluar` (`no_urut`, `no_surat`, `alamat_tujuan`, `tanggal_keluar`, `perihal`, `keterangan`, `instansi_tujuan`, `id_user`) VALUES
(1, 'BAI/2016/2/004', 'Jalan Pandanaran 30 Semarang', '2016-12-26', 'Kerja Sama Event', 'Untuk dana sponsorship', 'Markir Indonesia', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat_masuk`
--

DROP TABLE IF EXISTS `surat_masuk`;
CREATE TABLE `surat_masuk` (
  `no_urut` int(4) NOT NULL,
  `no_surat` varchar(20) NOT NULL,
  `instansi_pengirim` varchar(100) NOT NULL,
  `alamat_pengirim` varchar(100) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `perihal_surat` varchar(200) NOT NULL,
  `keterangan` varchar(300) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_log`
--

DROP TABLE IF EXISTS `system_log`;
CREATE TABLE `system_log` (
  `id_aktifitas` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `aktifitas` int(11) DEFAULT NULL,
  `tanggal_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `system_log`
--

INSERT INTO `system_log` (`id_aktifitas`, `id_user`, `aktifitas`, `tanggal_log`) VALUES
(1, 1, 1, '2016-09-29 05:17:56'),
(421, 1, 0, '2017-01-02 05:24:49'),
(422, 1, 1, '2017-01-02 05:24:53'),
(423, 1, 0, '2017-01-02 05:24:57'),
(424, 1, 1, '2017-01-02 05:40:26'),
(425, 1, 1, '2017-01-02 05:45:04'),
(426, 1, 1, '2017-01-02 05:45:43'),
(427, 1, 1, '2017-01-02 05:47:41'),
(428, 1, 1, '2017-01-02 06:05:53'),
(429, 1, 0, '2017-01-02 06:05:59'),
(430, 2, 1, '2017-01-02 06:06:49'),
(431, 2, 0, '2017-01-02 06:06:58'),
(432, 3, 1, '2017-01-02 06:07:05'),
(433, 3, 0, '2017-01-02 06:07:14'),
(434, 1, 1, '2017-01-02 09:36:03'),
(435, 1, 1, '2017-01-02 10:16:25'),
(436, 1, 0, '2017-01-02 10:26:22'),
(437, 1, 1, '2017-01-02 10:27:31'),
(438, 1, 1, '2017-01-03 00:22:15'),
(439, 1, 1, '2017-01-03 05:11:02'),
(440, 1, 1, '2017-01-03 05:13:40'),
(441, 1, 0, '2017-01-03 05:13:44'),
(442, 3, 1, '2017-01-03 05:13:50'),
(443, 3, 0, '2017-01-03 05:14:07'),
(444, 5, 1, '2017-01-03 05:14:12'),
(445, 5, 0, '2017-01-03 05:14:23'),
(446, 2, 1, '2017-01-03 05:14:34'),
(447, 2, 0, '2017-01-03 05:14:47'),
(448, 4, 1, '2017-01-03 05:14:59'),
(449, 4, 0, '2017-01-03 05:15:22'),
(450, 1, 1, '2017-01-03 05:15:27'),
(451, 1, 0, '2017-01-03 05:15:35'),
(452, 6, 1, '2017-01-03 05:18:36'),
(453, 3, 1, '2017-01-03 07:12:55'),
(454, 3, 0, '2017-01-03 07:13:00'),
(455, 1, 1, '2017-01-03 07:13:04'),
(456, 1, 1, '2017-01-04 01:26:24'),
(457, 1, 1, '2017-01-04 06:03:58'),
(458, 1, 0, '2017-01-04 06:04:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tentang_bai`
--

DROP TABLE IF EXISTS `tentang_bai`;
CREATE TABLE `tentang_bai` (
  `id_tentang` int(1) NOT NULL,
  `desk_singkat` text,
  `gambar` varchar(100) DEFAULT NULL,
  `desk_panjang` text,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tentang_bai`
--

INSERT INTO `tentang_bai` (`id_tentang`, `desk_singkat`, `gambar`, `desk_panjang`, `id_user`) VALUES
(1, 'sasasasa', NULL, 'asasasasasas', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `uang_kas`
--

DROP TABLE IF EXISTS `uang_kas`;
CREATE TABLE `uang_kas` (
  `id_uangkas` int(5) NOT NULL,
  `saldo_kas` int(11) NOT NULL,
  `uang_masuk` int(11) NOT NULL DEFAULT '0',
  `uang_keluar` int(11) NOT NULL DEFAULT '0',
  `tanggal_transaksi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `uang_kas_periode`
--

DROP TABLE IF EXISTS `uang_kas_periode`;
CREATE TABLE `uang_kas_periode` (
  `id_uang_bulanan` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `saldo_kas` int(11) NOT NULL DEFAULT '0',
  `uang_masuk` int(11) NOT NULL DEFAULT '0',
  `uang_keluar` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL,
  `tgl_transaksi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(100) DEFAULT NULL,
  `foto_user` varchar(100) NOT NULL,
  `email_user` varchar(150) DEFAULT NULL,
  `password_user` varchar(40) DEFAULT NULL,
  `role` enum('admin','pengurus','sekretaris','bendahara','superadmin','kaderisasi') DEFAULT NULL,
  `status_user` enum('0','1') DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `foto_user`, `email_user`, `password_user`, `role`, `status_user`) VALUES
(1, 'Indra Kusuma', 'b03e5-indra.jpg', 'indra@gmail.com', '9fb7cae6f9c3aacdb81c1e70b1f843e5', 'superadmin', '1'),
(2, 'Zakiy Anwar F', 'd7f82-zakiy.jpg', 'zakiy@gmail.com', '862f21d4fa92b0e26a9947db7527bb6b', 'pengurus', '1'),
(3, 'Mita Wanda', '', 'mita@gmail.com', 'bae3d929b274a4cd35c38fe92f059f1a', 'sekretaris', '1'),
(4, 'Kharis Hilmi Mutakarin', '', 'kharis@gmail.com', 'aa3adc6b720873f0a5a1c273bb75348b', 'kaderisasi', '1'),
(5, 'M Nurul Irfan', '', 'irfan@gmail.com', '24b90bc48a67ac676228385a7c71a119', 'bendahara', '1'),
(6, 'Nisa Haryanti', '', 'nisa@gmail.com', '5fad30428811fe378fd389cd7659a33c', 'admin', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `calon_anggota`
--
ALTER TABLE `calon_anggota`
  ADD PRIMARY KEY (`id_calon`),
  ADD UNIQUE KEY `nim` (`nim`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id_pesan`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `evaluasi_event`
--
ALTER TABLE `evaluasi_event`
  ADD PRIMARY KEY (`id_eval`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id_event`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id_faq`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `gabung_bai`
--
ALTER TABLE `gabung_bai`
  ADD PRIMARY KEY (`id_gabung`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`id_galeri`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `info_contact`
--
ALTER TABLE `info_contact`
  ADD PRIMARY KEY (`id_info_contact`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `kategori_artikel`
--
ALTER TABLE `kategori_artikel`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kegiatan_rutin`
--
ALTER TABLE `kegiatan_rutin`
  ADD PRIMARY KEY (`id_kegitan_rutin`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `mutobaah`
--
ALTER TABLE `mutobaah`
  ADD PRIMARY KEY (`id_mutobaah`);

--
-- Indexes for table `mutobaah_bidang`
--
ALTER TABLE `mutobaah_bidang`
  ADD PRIMARY KEY (`id_bid_mutobaah`);

--
-- Indexes for table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id_periode`);

--
-- Indexes for table `peserta_event`
--
ALTER TABLE `peserta_event`
  ADD PRIMARY KEY (`id_pe`);

--
-- Indexes for table `sambutan_ketua`
--
ALTER TABLE `sambutan_ketua`
  ADD PRIMARY KEY (`id_sambutan`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `slideshow`
--
ALTER TABLE `slideshow`
  ADD PRIMARY KEY (`id_slide`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `surat_keluar`
--
ALTER TABLE `surat_keluar`
  ADD PRIMARY KEY (`no_urut`);

--
-- Indexes for table `surat_masuk`
--
ALTER TABLE `surat_masuk`
  ADD PRIMARY KEY (`no_urut`);

--
-- Indexes for table `system_log`
--
ALTER TABLE `system_log`
  ADD PRIMARY KEY (`id_aktifitas`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tentang_bai`
--
ALTER TABLE `tentang_bai`
  ADD PRIMARY KEY (`id_tentang`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `uang_kas`
--
ALTER TABLE `uang_kas`
  ADD PRIMARY KEY (`id_uangkas`);

--
-- Indexes for table `uang_kas_periode`
--
ALTER TABLE `uang_kas_periode`
  ADD PRIMARY KEY (`id_uang_bulanan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id_artikel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `calon_anggota`
--
ALTER TABLE `calon_anggota`
  MODIFY `id_calon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id_pesan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `evaluasi_event`
--
ALTER TABLE `evaluasi_event`
  MODIFY `id_eval` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id_event` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id_faq` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gabung_bai`
--
ALTER TABLE `gabung_bai`
  MODIFY `id_gabung` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `galeri`
--
ALTER TABLE `galeri`
  MODIFY `id_galeri` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `info_contact`
--
ALTER TABLE `info_contact`
  MODIFY `id_info_contact` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kategori_artikel`
--
ALTER TABLE `kategori_artikel`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kegiatan_rutin`
--
ALTER TABLE `kegiatan_rutin`
  MODIFY `id_kegitan_rutin` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mutobaah`
--
ALTER TABLE `mutobaah`
  MODIFY `id_mutobaah` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mutobaah_bidang`
--
ALTER TABLE `mutobaah_bidang`
  MODIFY `id_bid_mutobaah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `periode`
--
ALTER TABLE `periode`
  MODIFY `id_periode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `peserta_event`
--
ALTER TABLE `peserta_event`
  MODIFY `id_pe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sambutan_ketua`
--
ALTER TABLE `sambutan_ketua`
  MODIFY `id_sambutan` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `slideshow`
--
ALTER TABLE `slideshow`
  MODIFY `id_slide` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `surat_keluar`
--
ALTER TABLE `surat_keluar`
  MODIFY `no_urut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `surat_masuk`
--
ALTER TABLE `surat_masuk`
  MODIFY `no_urut` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_log`
--
ALTER TABLE `system_log`
  MODIFY `id_aktifitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=459;
--
-- AUTO_INCREMENT for table `tentang_bai`
--
ALTER TABLE `tentang_bai`
  MODIFY `id_tentang` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `uang_kas`
--
ALTER TABLE `uang_kas`
  MODIFY `id_uangkas` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uang_kas_periode`
--
ALTER TABLE `uang_kas_periode`
  MODIFY `id_uang_bulanan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `artikel`
--
ALTER TABLE `artikel`
  ADD CONSTRAINT `artikel_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `artikel_ibfk_2` FOREIGN KEY (`id_kategori`) REFERENCES `kategori_artikel` (`id_kategori`);

--
-- Ketidakleluasaan untuk tabel `contact_us`
--
ALTER TABLE `contact_us`
  ADD CONSTRAINT `contact_us_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `event_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `faq`
--
ALTER TABLE `faq`
  ADD CONSTRAINT `faq_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `gabung_bai`
--
ALTER TABLE `gabung_bai`
  ADD CONSTRAINT `gabung_bai_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `galeri`
--
ALTER TABLE `galeri`
  ADD CONSTRAINT `galeri_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `inventaris`
--
ALTER TABLE `inventaris`
  ADD CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `kegiatan_rutin`
--
ALTER TABLE `kegiatan_rutin`
  ADD CONSTRAINT `kegiatan_rutin_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `sambutan_ketua`
--
ALTER TABLE `sambutan_ketua`
  ADD CONSTRAINT `sambutan_ketua_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `slideshow`
--
ALTER TABLE `slideshow`
  ADD CONSTRAINT `slideshow_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `system_log`
--
ALTER TABLE `system_log`
  ADD CONSTRAINT `system_log_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `tentang_bai`
--
ALTER TABLE `tentang_bai`
  ADD CONSTRAINT `tentang_bai_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
