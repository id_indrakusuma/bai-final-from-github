<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$title;?>
        <small>BAI Online System</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
          <div class="box-header">

              <?php
                $info = $this->session->flashdata('info');
                if (isset($info)) {
                  ?>
                    <div class="callout callout-warning">
                      <h4><?=$info;?></h4>
                    </div>
                  <?php
                }
              ?>
              <h3 class="box-title">User Aktif BAI</h3>
              <a href="<?=base_url();?>Admin/addUser" title="" class="btn btn-sm btn-primary pull-right"><i class="fa fa-plus-circle"> </i> Tambah User </a>


            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="user_log" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th>Nama User</th>
                  <th>Jabatan</th>
                  <th>Aktifitas Terakhir</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                
                <tbody>
                  <?php foreach ($this->Main_model->getDaftarUserBai() as $data) {
                    //-> Kondisi tambahan untuk merubah angka menjadi text yang kita inginkan
                    ?>
                      <tr>
                        <td><?=$data['nama_user'];?></td>
                        <td><?=$data['role'];?></td>
                        <td><?=$data['tanggal_log'];?></td>
                        <td>
                          <a href="<?=base_url(); ?>Admin/deleteUser/<?=$data['id_user'];?>" class="btn btn-xs btn-danger" title="Ubah Artikel"><i class="fa fa-eraser"></i> Hapus</a>
                          <a href="<?=base_url(); ?>Admin/editUser/<?=$data['id_user'];?>" class="btn btn-xs btn-success" title="Hapus Artikel"><i class="fa fa-pencil"></i> Ubah</a> 
                        </td>
                      
                      </tr>
                    <?php
                } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>